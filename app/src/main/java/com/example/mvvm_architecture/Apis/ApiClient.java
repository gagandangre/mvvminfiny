package com.example.mvvm_architecture.Apis;


import android.os.Build;

import androidx.annotation.RequiresApi;




import java.text.DateFormat;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiClient {

    private Map<String, Interceptor> apiAuthorizations;
    private OkHttpClient.Builder okBuilder;
    private Retrofit.Builder adapterBuilder;
    private JSON json;

    public ApiClient() {
        //apiAuthorizations = new LinkedHashMap<String, Interceptor>();
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        createDefaultAdapter();

//    }
    }

    public ApiClient(String[] authNames) {
        this();

        if (authNames.length > 0)
            for (String authName : authNames) {

                if (authName == null || authName.isEmpty()) {
                    throw new RuntimeException("auth name \"" + authName + "\" not found in available auth names");
                }
            }
    }

    /**
     * Basic constructor for single auth name
     *
     * @param authName Authentication name
     */
    public ApiClient(String authName) {
        this(new String[]{authName});
    }


/*  public ApiClient(String authName, String apiKey) {
    this(authName);
    this.setApiKey(apiKey);
  }*/



  /*@RequiresApi(api = Build.VERSION_CODES.O)
  public void createDefaultAdapterSelect(boolean isNotDefault) {
    if(isNotDefault){
      baseUrl="https://restcountries.herokuapp.com/api/v1/";
    }
    createDefaultAdapter();
  }*/

    //  @RequiresApi(api = Build.VERSION_CODES.O)
    public void createDefaultAdapter() {


        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            json = new JSON();
//        }
        okBuilder = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .connectTimeout(200, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS);
    /*okBuilder = new OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS);*/
//https://restcountries.herokuapp.com/api/v1
        // String baseUrl = "https://restcountries.eu/rest/v2/";
        // String baseUrl =   "http://192.168.29.125:5354/api";
//        if (!baseUrl.endsWith("/"))
//            baseUrl = baseUrl + "/";

        adapterBuilder = new Retrofit
                .Builder()
                .baseUrl("https://astrowize.infiny.dev/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonCustomConverterFactory.create(json.getGson()));
    }


    public <S> S createService(Class<S> serviceClass) {
        return adapterBuilder
                .client(okBuilder.build())
                .build()
                .create(serviceClass);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public ApiClient setDateFormat(DateFormat dateFormat) {
        this.json.setDateFormat(dateFormat);
        return this;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public ApiClient setSqlDateFormat(DateFormat dateFormat) {
        this.json.setSqlDateFormat(dateFormat);
        return this;
    }

/*  public ApiClient setOffsetDateTimeFormat(DateTimeFormatter dateFormat) {
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      this.json.setOffsetDateTimeFormat(dateFormat);
//    }
    return this;
  }

  public ApiClient setLocalDateFormat(DateTimeFormatter dateFormat) {
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      this.json.setLocalDateFormat(dateFormat);
//    }
    return this;
  }*/



/*  public ApiClient setApiKey(String apiKey) {
    for(Interceptor apiAuthorization : apiAuthorizations.values()) {
      if (apiAuthorization instanceof ApiKeyAuth) {
        ApiKeyAuth keyAuth = (ApiKeyAuth) apiAuthorization;
        keyAuth.setApiKey(apiKey);
        return this;
      }
    }
    return this;
  }*/
    public ApiClient addAuthorization(String authName, Interceptor authorization) {
        if (apiAuthorizations.containsKey(authName)) {
            throw new RuntimeException("auth name \"" + authName + "\" already in api authorizations");
        }
        apiAuthorizations.put(authName, authorization);
        okBuilder.addInterceptor(authorization);
        return this;
    }

    public Map<String, Interceptor> getApiAuthorizations() {
        return apiAuthorizations;
    }

    public ApiClient setApiAuthorizations(Map<String, Interceptor> apiAuthorizations) {
        this.apiAuthorizations = apiAuthorizations;
        return this;
    }

    public Retrofit.Builder getAdapterBuilder() {
        return adapterBuilder;
    }

    public ApiClient setAdapterBuilder(Retrofit.Builder adapterBuilder) {
        this.adapterBuilder = adapterBuilder;
        return this;
    }

    public OkHttpClient.Builder getOkBuilder() {
        return okBuilder;
    }

    public void addAuthsToOkBuilder(OkHttpClient.Builder okBuilder) {
        for (Interceptor apiAuthorization : apiAuthorizations.values()) {
            okBuilder.addInterceptor(apiAuthorization);
        }
    }

    /**
     * Clones the okBuilder given in parameter, adds the auth interceptors and uses it to configure the Retrofit
     *
     * @param okClient An instance of OK HTTP client
     */
    public void configureFromOkclient(OkHttpClient okClient) {
        this.okBuilder = okClient.newBuilder();
        addAuthsToOkBuilder(this.okBuilder);
    }
}

