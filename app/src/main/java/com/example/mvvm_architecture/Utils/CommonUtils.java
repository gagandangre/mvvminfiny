package com.example.mvvm_architecture.Utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mvvm_architecture.Apis.ApiClient;
import com.example.mvvm_architecture.Apis.UserApi;
import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.HttpException;

import static android.content.Context.MODE_PRIVATE;

public class CommonUtils {

    private static SessionManager sessionManager;
    private static SharedPreferences sharedPreferences;

    public static String token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImdhZ2FuLmRAaW5maW55LmluIiwidXNlcl9pZCI6IjVmZWEwZjNlMzA0ZDIzN2RhOTA1Y2U5YSIsInVzZXJfdHlwZSI6ImNvbnN1bWVyIiwiaXNNb2JpbGUiOnRydWUsImV4cCI6MTgzODAxMDE4OSwiaWF0IjoxNjIyMDEwMTg5fQ.s_0r1q3bD3IsFL1wglLeDtgrOYRcwTTFiQUKlZxUBrk";

    public  static void hideKeyboard(Context context, View view){
        InputMethodManager i = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        i.hideSoftInputFromWindow(view.getWindowToken(),0);
    }


    public static boolean isNetworkAvailable(Context context){

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();

    }

    public  static  boolean checkInternetConnection(final  Context context){


        if(!isNetworkAvailable(context)){
            AlertDialog.Builder alertDialog;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                alertDialog = new AlertDialog.Builder(context);
            }else{
                alertDialog = new AlertDialog.Builder(context);
            }
            alertDialog.setTitle("Network");
            alertDialog.setMessage("Check your Internet connection");
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });

            try {
                AlertDialog alertDialog1 = alertDialog.show();
                alertDialog1.show();
            }catch (Exception exception){
                Log.i("exception","badWindowToken-----"+exception);
            }

            return  false;
        }
        else {
            return true;
        }

    }

    public static String getStringFile(File f) throws IOException {

        String encodedFile= "", lastVal;
        Log.i("lastVal","getAbsolutePath====>"+f.getAbsolutePath());
        try {
            InputStream inputStream = new FileInputStream(f.getAbsolutePath());
            Log.i("lastVal","inputStream====>"+inputStream);

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile =  output.toString();
        }
        catch (FileNotFoundException e1 ) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }/*finally {
            inputStream.close();
        }*/

        lastVal = encodedFile;

        Log.i("lastVal","lastVal====>"+lastVal);

        return lastVal;
    }


    public  static String convertDateFormat(String dateString, String format, String newFormat){

        String date= dateString;
        SimpleDateFormat spf = new SimpleDateFormat(format, Locale.ENGLISH);
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date newDate = null;
        try {
            newDate = spf.parse(date);

        }catch (ParseException e){
            e.printStackTrace();
        }

        spf = new SimpleDateFormat(newFormat,Locale.ENGLISH);
        date = spf.format(newDate);
        return date;
    }


    public  static Date getChatStartTime(String dateString, String format, String newFormat){

        String date= dateString;
        SimpleDateFormat spf = new SimpleDateFormat(format, Locale.ENGLISH);
        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date newDate = null;
        try {
            newDate = spf.parse(date);

        }catch (ParseException e){
            e.printStackTrace();
        }

        spf = new SimpleDateFormat(newFormat,Locale.ENGLISH);
        date = spf.format(newDate);
        return newDate;
    }

    public  static ProgressDialog showProgressdialog(Context context){

        ProgressDialog progressDialog = new ProgressDialog(context);
        if(!progressDialog.isShowing()){
            progressDialog.show();

        }
        if(progressDialog.getWindow() !=null){
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        progressDialog.setContentView(R.layout.layout_progressbar);



        progressDialog.setIndeterminate(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return progressDialog;

    }

    public static void fadeIn(ImageView imageView) {

        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f);
        objectAnimator.setDuration(800L);
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                fadeOut(imageView);
            }
        });
        objectAnimator.start();
    }




    public static void fadeOut(ImageView imageView) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imageView, "alpha", 1f, 0f);
        objectAnimator.setDuration(800L);
        objectAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                fadeIn(imageView);
            }
        });
        objectAnimator.start();
    }

    public static void fadeInOnce(RelativeLayout imageView) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f);
        objectAnimator.setDuration(800L);

        objectAnimator.start();
    }

    public static void hideLoading(ProgressDialog mProgressDialog) {
        Log.i("mProgressDialog"," ===== > "+mProgressDialog );
        if  ( mProgressDialog != null ) {
            mProgressDialog.setCanceledOnTouchOutside(true);
            if(mProgressDialog.isShowing()){
                Log.i("mProgressDialog"," ==isShowing=== > "+mProgressDialog );
                mProgressDialog.dismiss();
                Log.i("mProgressDialog"," ==isShowing=== > "+mProgressDialog.isShowing() );
            }
        }
    }


    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String convertTwoDecimal(String amount){
         return String.format("%.2f", Double.parseDouble(amount));
    }


    public static boolean isValidPhone(String email) {
        if(email.trim().length()==10){
            return true;
        }
        return false;
    }

    public  static UserApi getApiInterface( ){

//        Retrofit retrofit = new ApiClient().getRetrofit();
        /*if(!isNetworkAvailable(context)){
            checkInternetConnection(context);
        }*/
        ApiClient apiClient = new ApiClient();
        UserApi userApi = apiClient.createService(UserApi.class);
        return userApi;
    }

    public static void openSnackbar(View view, String msg){

        Toast.makeText(view.getContext(), msg, Toast.LENGTH_SHORT).show();


    }
    public static long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();
        System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;
        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;
        long elapsedSeconds = different / secondsInMilli;
        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
        return elapsedDays;
    }

    public static void setRecyclerView(Context context, RecyclerView recyClerPanchangDetails, Object panchangDetailsAdapter) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyClerPanchangDetails.setLayoutManager(linearLayoutManager);
        recyClerPanchangDetails.setAdapter((RecyclerView.Adapter) panchangDetailsAdapter);

    }



    public static String getFCMToken(){
        String token=null;
      /*  FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });*/

      return token;
    }

    public static String getName(String firstname, String lastName) {
        return  firstname!=null ? lastName!=null?(firstname+" "+lastName) : firstname : "--";
    }

    public static String capsFirstLetter(String boyName) {
        String upperString = boyName.substring(0, 1).toUpperCase() + boyName.substring(1).toLowerCase();
        return upperString;
    }


    public void checkDiffFromCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy");
        String getCurrentDate = sdf.format(c.getTime());
        long monthsBetween = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            monthsBetween = ChronoUnit.MONTHS.between(
                    LocalDate.parse(getCurrentDate).withDayOfMonth(1),
                    LocalDate.parse("").withDayOfMonth(1));
        }
        System.out.println(monthsBetween);
    }

    public  static String convertTheDateToFormat(String dateString, String format,String newFormat){
        String date=dateString;
        SimpleDateFormat spf=new SimpleDateFormat(format,Locale.ENGLISH);

//        spf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date newDate= null;
        try {

            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        spf= new SimpleDateFormat(newFormat,Locale.ENGLISH);
        date = spf.format(newDate);
        return date;
    }


    public  static String convertTheDateToFormatGMT(String dateString, String format,String newFormat){
        String date=dateString;
        SimpleDateFormat spf=new SimpleDateFormat(format, Locale.US);

        Date newDate= null;
        try {

            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        spf= new SimpleDateFormat(newFormat);
        date = spf.format(newDate);
        return date;
    }





    public static boolean checkInternetConnetion(final Context context){

        sessionManager = new SessionManager(context);
        sharedPreferences = context.getSharedPreferences(sessionManager.PREF_NAME, MODE_PRIVATE);

        if(!isNetworkAvailable(context)) {
            AlertDialog.Builder builder;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context);
            } else {*/

            builder = new AlertDialog.Builder(context,R.style.AlertDialogCustom);

            builder.setTitle(context.getResources().getString(R.string.Alert));
            builder.setMessage("Please check your internet connection and try again.");
            builder.setCancelable(false);
            builder.setPositiveButton(context.getResources().getString(R.string.try_later), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
//                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

//                        sessionManager.saveTryAgain("try_Later");
                        dialog.dismiss();

                }
            });

            builder.setNeutralButton(context.getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   /* if(!isNetworkAvailable(context)) {
                        dialog.dismiss();
                    }*/

                    if(isNetworkAvailable(context)) {
                        dialog.dismiss();

                    }else {
                        checkInternetConnetion(context);
                    }

                }
            });
            // builder.setIcon(R.drawable.ic_icon_transaction_pending);
        /*    if(sharedPreferences.getString(sessionManager.TRYAGAIN,null)!=null){

                Log.i("TRYAGAIN","TRYAGAIN=============="+sharedPreferences.getString(sessionManager.TRYAGAIN,null));

                if(!sharedPreferences.getString(sessionManager.TRYAGAIN,null).equals("try_Later")){
                    AlertDialog dialog = builder.show();
                    dialog.show();
                }
            }*/



            /*AlertDialog dialog = builder.show();
            dialog.show();*/
            return false;
        }else{
//            sessionManger.saveTryAgain("ff");
            return true;
        }

    }

    public static boolean checkInternetConnetionOffline(final Context context){

        sessionManager = new SessionManager(context);
        sharedPreferences = context.getSharedPreferences(sessionManager.PREF_NAME, MODE_PRIVATE);

        if(!isNetworkAvailable(context)) {
            AlertDialog.Builder builder;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context);
            } else {*/

            builder = new AlertDialog.Builder(context,R.style.AlertDialogCustom);

            builder.setTitle(context.getResources().getString(R.string.Alert));
            builder.setMessage("Please check your internet connection and try again.");
            builder.setCancelable(false);
            builder.setPositiveButton(context.getResources().getString(R.string.try_later), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
//                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

                    dialog.dismiss();

                }
            });

            builder.setNeutralButton(context.getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                   /* if(!isNetworkAvailable(context)) {
                        dialog.dismiss();
                    }*/

                    if(isNetworkAvailable(context)) {
                        dialog.dismiss();

                    }else {
                        checkInternetConnetion(context);
                    }

                }
            });
            // builder.setIcon(R.drawable.ic_icon_transaction_pending);



            AlertDialog dialog = builder.show();
            dialog.show();
            return false;
        }else{
//            sessionManger.saveTryAgain("ff");
            return true;
        }

    }




    public static void showError (Throwable throwable,Context context){
        Log.i("showError", " showError from utils " + context.getPackageName().getClass() + " with error " + throwable.getMessage());
        checkInternetConnetion(context);
        try {
            if(throwable instanceof SocketTimeoutException){
                Log.d("SocketTimeoutException","SocketTimeoutException --- > ");
                Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
            HttpException error = (HttpException)throwable;
            String errorBody = null;
            errorBody = error.response().errorBody().string();
            Log.i("codeApplyFailure", "codeApplyFailure == > " + errorBody);
            JSONObject jObjError = new JSONObject(errorBody);
            if(jObjError.get("error")!=null){

                if(jObjError.get("title")!=null){
                    Toast.makeText(context, jObjError.get("title").toString(), Toast.LENGTH_SHORT).show();
                    if(jObjError.get("title").toString()!=null && jObjError.get("title").toString().equals("user not found")){
                        Intent intent =new Intent(context, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        SharedPreferences sharedPreferences=context.getSharedPreferences(SessionManager.PREF_NAME, MODE_PRIVATE);
                        sharedPreferences.edit().clear().commit();
                        //  new AsyncRoomDb(context).execute();
                        context.startActivity(intent);
                    }
                }else  if(jObjError.get("message")!=null){
                    Toast.makeText(context, jObjError.get("message").toString(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
//            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }


}
