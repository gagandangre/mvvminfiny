package com.example.mvvm_architecture.UI.BottomNavJetPack.View.Help.view;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.Models.FromId;
import com.example.mvvm_architecture.UI.Models.UserRating;
import com.example.mvvm_architecture.UI.Pagination.View.BlogAdapter;
import com.example.mvvm_architecture.Utils.CommonUtils;
import com.example.mvvm_architecture.databinding.BlogItemBinding;
import com.example.mvvm_architecture.databinding.HelpItemBinding;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.MyViewHolder> {

    Context context;
    List<UserRating> userRatingList;


    public HelpAdapter(Context context, List<UserRating> userRatingList) {
        this.context = context;
        this.userRatingList = new ArrayList<>();
        this.userRatingList = userRatingList;
        Log.i(getClass().getName(),"HelpAdapter---->"+userRatingList.size());
    }

    @NonNull
    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new HelpAdapter.MyViewHolder(HelpItemBinding.inflate(LayoutInflater.from(parent.getContext())));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MyViewHolder holder, int position) {

        Log.i(getClass().getName(),"onBindViewHolder---->");
        holder.helpItemBinding.txtName.setText("name:-" +position+"");

        /*UserRating userRating = userRatingList.get(position);
        FromId userData = userRating.getFromId();

        Log.i(getClass().getName(),"userRating--->"+userRating);
        Log.i(getClass().getName(),"userRating--->"+userRating.getRatingValue());

        if(userRating.getRatingValue()!=null){
            holder.helpItemBinding.ratingStarOverall.setRating(Float.parseFloat(userRating.getRatingValue()));
        }

        holder.helpItemBinding.txtDate.setText(CommonUtils.convertDateFormat(userRating.getCreatedAt(),"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'","dd/MM/yyyy HH:mm a"));

        String name ="";

        if(userData!=null) {


            if (userData.getFirstName() != null) {

                name = userData.getFirstName();

            } else {
                name = "N.A";
            }


        } else {
            name = "N.A";
        }

        holder.helpItemBinding.txtName.setText(name);

        if(userRating.getDescription()!=null) {
            holder.helpItemBinding.txtDescription.setText(userRating.getDescription());
        }else {
            holder.helpItemBinding.txtDescription.setText("");
        }*/


    }

    @Override
    public int getItemCount() {
        return userRatingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        HelpItemBinding helpItemBinding;

        public MyViewHolder(@NonNull HelpItemBinding helpItemBinding) {
            super(helpItemBinding.getRoot());
            this.helpItemBinding = helpItemBinding;

        }
    }
}
