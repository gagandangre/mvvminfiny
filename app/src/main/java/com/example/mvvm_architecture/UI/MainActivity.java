package com.example.mvvm_architecture.UI;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mvvm_architecture.UI.BottomNavJetPack.View.BottomNavigationActivity;
import com.example.mvvm_architecture.UI.EditProfile.View.EditProfileActivity;
import com.example.mvvm_architecture.UI.Login.View.LoginActivity;
import com.example.mvvm_architecture.UI.Pagination.View.BlogActivity;
import com.example.mvvm_architecture.UI.PaginationPager3Library.View.MyWalletActivity;
import com.example.mvvm_architecture.UI.RoomDB.view.RoomActivity;
import com.example.mvvm_architecture.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding activityMainBinding;
    Context context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        activityMainBinding =ActivityMainBinding.inflate(getLayoutInflater());
        View view = activityMainBinding.getRoot();
        setContentView(view);
        context =this;
        setListeners();

    }

    private void setListeners() {

        activityMainBinding.relPagination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(getClass().getName(),"paginationClick---->");

                Intent intent = new Intent(context, BlogActivity.class);
                startActivity(intent);

            }
        });

        activityMainBinding.relPaginationLibrary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyWalletActivity.class);
                startActivity(intent);
            }
        });

        activityMainBinding.relEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditProfileActivity.class);
                startActivity(intent);
            }
        });

        activityMainBinding.relLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });

        activityMainBinding.relFragmentNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BottomNavigationActivity.class);
                startActivity(intent);
            }
        });

        activityMainBinding.relRoomDbMvvm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RoomActivity.class);
                startActivity(intent);
            }
        });


    }
}