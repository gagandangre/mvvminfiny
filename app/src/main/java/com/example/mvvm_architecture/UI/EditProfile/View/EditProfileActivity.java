package com.example.mvvm_architecture.UI.EditProfile.View;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.mvvm_architecture.UI.EditProfile.ViewModel.EditProfileViewModel;
import com.example.mvvm_architecture.UI.Models.UserModel;
import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.Utils.CommonUtils;
import com.example.mvvm_architecture.Utils.CropImageActivity;
import com.example.mvvm_architecture.Utils.FileUtils;
import com.example.mvvm_architecture.databinding.ActivityEditProfileBinding;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EditProfileActivity extends AppCompatActivity {

    ActivityEditProfileBinding activityEditProfileBinding;
    Context context;
    EditProfileViewModel editProfileViewModel;
    private ProgressDialog progressBar;
    private static final int SELECT_PICTURES_MULTIPLE = 123;
    private static final int REQUEST_IMAGE_CAPTURE = 854;
    private static final int CAMERA_REQUEST = 18;
    private final int PICK_PDF_REQUEST = 154;
    private static final int CROP_ACTIVITY = 15;  //
    private Intent intent;
    private Uri imageUri;
    private String mCurrentPhotoPath;
    ActivityResultLauncher<Intent> imagePickerLaucher;
    ActivityResultLauncher<Intent> pdfPickerLaucher;
    ActivityResultLauncher<Intent> cropImageLaucher;
    private String imageString;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setInit();
        setListeners();

        getResultFromActivity();// alternative to onActivityResult

    }

    private void getResultFromActivity() {

        imagePickerLaucher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        Log.i(getClass().getName(),"getResultCode---->"+result.getResultCode());
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();

                            Log.i(getClass().getName(),"data---->"+imageUri);

                            if(imageUri!=null) {
                                Log.i("onActivity",  " ------------- > " + imageUri);
                                startCropActivity(imageUri);
                            }

                        }
                    }
                });

        cropImageLaucher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            // There are no request codes
                            Intent data = result.getData();
                            if (data != null){
                                Uri imageUri = data.getParcelableExtra("sourceuri");
                                // Uri imageUri = data.getData();
                                Log.i("CROP_ACTIVITY", " CROP_ACTIVITY bitmap ==> " +  imageUri);
                                if(imageUri!=null){
                                    try {
                                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
                                        File file = FileUtils.getFile(context, imageUri);
                                        Log.i("CROP_ACTIVITY", " CROP_ACTIVITY bitmap ==> " +  imageUri +" Bitmap  "+ bitmap );
                                        Picasso.get().load(imageUri)
                                                .into(activityEditProfileBinding.imgProfile);
                                        String imageBase64 = CommonUtils.getStringFile(file);
                                        Log.i("CROP_ACTIVITY", " CROP_ACTIVITY imageBase64 ==> " + imageBase64);
                                        imageString="data:image/jpeg;base64,"+imageBase64;

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }

                        }
                    }
                });
    }

    private void startCropActivity(@NonNull Uri uri) {

        Intent intent=new Intent(context, CropImageActivity.class);
        // Log.i(TAG,"startCropActivity  -- > "+uri.toString());
        intent.putExtra("sourceUri",uri.toString());
        cropImageLaucher.launch(intent);
        //overridePendingTransition(R.anim.right_in,R.anim.left_out);
    }

    private void setInit() {
        context = this;

        activityEditProfileBinding =ActivityEditProfileBinding.inflate(getLayoutInflater());
        View view =  activityEditProfileBinding.getRoot();
        setContentView(view);

        editProfileViewModel = new ViewModelProvider(this).get(EditProfileViewModel.class);
        editProfileViewModel.getUserModelLiveData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel userModel) {
                Toast.makeText(context, userModel.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });

        editProfileViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoading) {
                Log.i(getClass().getName(),"getIsLoading--->"+isLoading);
                if (isLoading){
                    progressBar = CommonUtils.showProgressdialog(context);
                } else {
                    CommonUtils.hideLoading(progressBar);
                }
            }
        });

        editProfileViewModel.getToastObserver().observe(this, message -> {
            Log.i(getClass().getName(),"message====>"+message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        });


    }

    private void setListeners() {

        activityEditProfileBinding.imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openFileUploadDailog();
            }
        });

        activityEditProfileBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editProfileViewModel.editProfileApi(activityEditProfileBinding.edtFirstName.getText().toString()
                ,activityEditProfileBinding.edtEmail.getText().toString(),activityEditProfileBinding.edtPhone.getText().toString(),imageString);
            }
        });



    }

    private void openFileUploadDailog() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context
                    , new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}
                    , REQUEST_IMAGE_CAPTURE);
        } else {

            final CharSequence[] items = {"Camera", "Choose from gallery"};
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
            builder.setTitle("Add File ");
            builder.setItems(items, (dialog, item) -> {

                switch (items[item].toString()) {

                    case "Camera":
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context
                                    , new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                                    , REQUEST_IMAGE_CAPTURE);
                        } else {
                            openCamera();

                        }

                        break;

                    case "Choose from gallery":
                        //ACTION_GET_CONTENT
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions((Activity) context
                                    , new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}
                                    , REQUEST_IMAGE_CAPTURE);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                            // intent.setType("image/*");
                            //  Allows any image file type. Change * to specific extension to limit it
                            //**These following line is the important one!
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                            imagePickerLaucher.launch(intent);
                            //SELECT_PICTURES is simply a global int used to check the calling intent in onActivityResult
                        }
                        break;


                }
            });
            builder.show();
        }

    }

    private void openCamera() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_IMAGE_CAPTURE);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageUri = FileProvider.getUriForFile(context,
                    context.getApplicationContext().getPackageName()
                            + ".provider",
                    createImageFile());
            Log.i(getClass().getName(), "imageURI :" + imageUri);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            imagePickerLaucher.launch(intent);

        } else {

            Log.i(getClass().getName(),"Else----------------->");
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageUri = Uri.fromFile(createImageFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            imagePickerLaucher.launch(intent);
        }
    }

    private File createImageFile() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents

        Log.i(getClass().getName(), "createImage file log  " + image);
        return image;
    }
}