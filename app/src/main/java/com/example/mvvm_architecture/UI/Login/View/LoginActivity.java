package com.example.mvvm_architecture.UI.Login.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.mvvm_architecture.UI.Login.ViewModel.LoginViewModel;
import com.example.mvvm_architecture.UI.Models.UserModel;
import com.example.mvvm_architecture.Utils.CommonUtils;
import com.example.mvvm_architecture.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding activityLoginBinding;
    Context context;
    LoginViewModel loginViewModel;
    ProgressDialog progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setInit();
        setListeners();

    }

    private void setListeners() {

        activityLoginBinding.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginViewModel.loginApi(activityLoginBinding.edtMobNumber.getText().toString(),
                        activityLoginBinding.edtPassword.getText().toString());

            }
        });

    }

    private void setInit() {
        activityLoginBinding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = activityLoginBinding.getRoot();
        setContentView(view);

        context = this;

        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);

        loginViewModel.getUserModelLiveData().observe(this, new Observer<UserModel>() {
            @Override
            public void onChanged(UserModel userModel) {

                Log.i(getClass().getName(),"userModel---"+userModel);
                Toast.makeText(context, userModel.getTitle(), Toast.LENGTH_SHORT).show();

            }
        });

        loginViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isFetching) {

               if(isFetching){
                   progressBar = CommonUtils.showProgressdialog(context);
               }else {
                   CommonUtils.hideLoading(progressBar);
               }

            }
        });

        loginViewModel.getToastMessageObserver().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String toastMessage) {

                Toast.makeText(context,toastMessage , Toast.LENGTH_SHORT).show();

            }
        });


    }
}