package com.example.mvvm_architecture.UI.RoomDB.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.databinding.ActivityRoomBinding;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class RoomActivity extends AppCompatActivity {


    ActivityRoomBinding activityRoomBinding;
    Context context;
    private MyAdapter myAdapter;
    private String[] tabName ={"New", "All Users"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setInit();
        setListeners();

    }

    private void setListeners() {



    }

    private void setInit() {
        context = this;
        activityRoomBinding =ActivityRoomBinding.inflate(getLayoutInflater());
        View view = activityRoomBinding.getRoot();
        setContentView(view);

        activityRoomBinding.tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        activityRoomBinding.tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#4426A8"));
        activityRoomBinding.tabLayout.setSelectedTabIndicatorHeight((int) (5 * getResources().getDisplayMetrics().density));
        activityRoomBinding.tabLayout.setTabTextColors(Color.parseColor("#727272"), Color.parseColor("#4426A8"));


        myAdapter = new MyAdapter(this);

        activityRoomBinding.viewPager.setAdapter(myAdapter);

        activityRoomBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                activityRoomBinding.viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        new TabLayoutMediator(activityRoomBinding.tabLayout, activityRoomBinding.viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(tabName[position]);
            }
        }).attach();


    }

}
