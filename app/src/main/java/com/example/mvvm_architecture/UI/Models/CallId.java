package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("room_status")
    @Expose
    private String roomStatus;
    @SerializedName("consumer_status")
    @Expose
    private String consumerStatus;
    @SerializedName("astrologer_status")
    @Expose
    private String astrologerStatus;
    @SerializedName("consumer_name")
    @Expose
    private String consumerName;
    @SerializedName("astrologer_name")
    @Expose
    private String astrologerName;
    @SerializedName("consumer_call_id")
    @Expose
    private String consumerCallId;
    @SerializedName("astrologer_call_id")
    @Expose
    private String astrologerCallId;
    @SerializedName("consumer_duration")
    @Expose
    private Integer consumerDuration;
    @SerializedName("astrologer_duration")
    @Expose
    private Integer astrologerDuration;
    @SerializedName("cancelled_by")
    @Expose
    private String cancelledBy;
    @SerializedName("call_started")
    @Expose
    private Boolean callStarted;
    @SerializedName("call_rate")
    @Expose
    private Double callRate;
    @SerializedName("client_call_rate")
    @Expose
    private Double clientCallRate;
    @SerializedName("schedule_name")
    @Expose
    private String scheduleName;
    @SerializedName("consumer_id")
    @Expose
    private String consumerId;
    @SerializedName("astrologer_id")
    @Expose
    private String astrologerId;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("call_audio_video")
    @Expose
    private String callAudioVideo;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("room_sid")
    @Expose
    private String roomSid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(String roomStatus) {
        this.roomStatus = roomStatus;
    }

    public String getConsumerStatus() {
        return consumerStatus;
    }

    public void setConsumerStatus(String consumerStatus) {
        this.consumerStatus = consumerStatus;
    }

    public String getAstrologerStatus() {
        return astrologerStatus;
    }

    public void setAstrologerStatus(String astrologerStatus) {
        this.astrologerStatus = astrologerStatus;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }

    public String getAstrologerName() {
        return astrologerName;
    }

    public void setAstrologerName(String astrologerName) {
        this.astrologerName = astrologerName;
    }

    public String getConsumerCallId() {
        return consumerCallId;
    }

    public void setConsumerCallId(String consumerCallId) {
        this.consumerCallId = consumerCallId;
    }

    public String getAstrologerCallId() {
        return astrologerCallId;
    }

    public void setAstrologerCallId(String astrologerCallId) {
        this.astrologerCallId = astrologerCallId;
    }

    public Integer getConsumerDuration() {
        return consumerDuration;
    }

    public void setConsumerDuration(Integer consumerDuration) {
        this.consumerDuration = consumerDuration;
    }

    public Integer getAstrologerDuration() {
        return astrologerDuration;
    }

    public void setAstrologerDuration(Integer astrologerDuration) {
        this.astrologerDuration = astrologerDuration;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public Boolean getCallStarted() {
        return callStarted;
    }

    public void setCallStarted(Boolean callStarted) {
        this.callStarted = callStarted;
    }

    public Double getCallRate() {
        return callRate;
    }

    public void setCallRate(Double callRate) {
        this.callRate = callRate;
    }


    public Double getClientCallRate() {
        return clientCallRate;
    }

    public void setClientCallRate(Double clientCallRate) {
        this.clientCallRate = clientCallRate;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getAstrologerId() {
        return astrologerId;
    }

    public void setAstrologerId(String astrologerId) {
        this.astrologerId = astrologerId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getCallAudioVideo() {
        return callAudioVideo;
    }

    public void setCallAudioVideo(String callAudioVideo) {
        this.callAudioVideo = callAudioVideo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getRoomSid() {
        return roomSid;
    }

    public void setRoomSid(String roomSid) {
        this.roomSid = roomSid;
    }

}

