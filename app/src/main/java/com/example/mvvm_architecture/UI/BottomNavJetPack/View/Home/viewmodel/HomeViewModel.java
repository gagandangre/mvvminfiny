package com.example.mvvm_architecture.UI.BottomNavJetPack.View.Home.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private static final String NAME_KEY = "name";
    SavedStateHandle savedStateHandle;


    public HomeViewModel(SavedStateHandle savedStateHandle) {
        this.savedStateHandle =savedStateHandle;
    }

    // Expose an immutable LiveData
    public LiveData<String> getName() {
        // getLiveData obtains an object that is associated with the key wrapped in a LiveData
        // so it can be observed for changes.
        return savedStateHandle.getLiveData(NAME_KEY);
    }

    public void saveNewName(String newName) {
        // Sets a new value for the object associated to the key. There's no need to set it
        // as a LiveData.
        savedStateHandle.set(NAME_KEY, newName);
//        savedStateHandle.set("f",Use);
    }
}
