package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceReqId {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("service_id")
    @Expose
    private ServiceId serviceId;
    @SerializedName("service_time")
    @Expose
    private String serviceTime;
    @SerializedName("consumer_id")
    @Expose
    private String consumerId;
    @SerializedName("rate")
    @Expose
    private Integer rate;
    @SerializedName("product_number")
    @Expose
    private String productNumber;
    @SerializedName("pay_order_id")
    @Expose
    private String payOrderId;
    @SerializedName("pay_receipt")
    @Expose
    private String payReceipt;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("pay_payment_id")
    @Expose
    private String payPaymentId;
    @SerializedName("service_status")
    @Expose
    private String serviceStatus;
    @SerializedName("assigned_time")
    @Expose
    private String assignedTime;
    @SerializedName("astrologer_id")
    @Expose
    private String astrologerId;
    @SerializedName("otp")
    @Expose
    private Integer otp;
    @SerializedName("tip")
    @Expose
    private Double tip;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ServiceId getServiceId() {
        return serviceId;
    }

    public void setServiceId(ServiceId serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getPayOrderId() {
        return payOrderId;
    }

    public void setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
    }

    public String getPayReceipt() {
        return payReceipt;
    }

    public void setPayReceipt(String payReceipt) {
        this.payReceipt = payReceipt;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getPayPaymentId() {
        return payPaymentId;
    }

    public void setPayPaymentId(String payPaymentId) {
        this.payPaymentId = payPaymentId;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getAssignedTime() {
        return assignedTime;
    }

    public void setAssignedTime(String assignedTime) {
        this.assignedTime = assignedTime;
    }

    public String getAstrologerId() {
        return astrologerId;
    }

    public void setAstrologerId(String astrologerId) {
        this.astrologerId = astrologerId;
    }

    public Integer getOtp() {
        return otp;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public Double getTip() {
        return tip;
    }

    public void setTip(Double tip) {
        this.tip = tip;
    }
}