package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WalletBalanceData implements Serializable {


    @SerializedName("transaction_type")
    @Expose
    private String transaction_type;


    @SerializedName("transaction_amt")
    @Expose
    private String transaction_amt;

    @SerializedName("client_transaction_amt")
    @Expose
    private String clientTxn_amt;

    @SerializedName("pay_order_id")
    @Expose
    private String payOrderId;


    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    @SerializedName("pay_receipt")
    @Expose
    private String payReceipt;

    @SerializedName("call_id")
    @Expose
    private CallId callId;


    @SerializedName("chat_id")
    @Expose
    private ChatId chatId;

    public ChatId getChatId() {
        return chatId;
    }

    public void setChatId(ChatId chatId) {
        this.chatId = chatId;
    }

    public CallId getCallId() {
        return callId;
    }

    public void setCallId(CallId callId) {
        this.callId = callId;
    }


    @SerializedName("astrologer_id")
    @Expose
    private AstrologerId astrologer_id;

    public AstrologerId getAstrologer_id() {
        return astrologer_id;
    }
    public void setAstrologer_id(AstrologerId astrologer_id) {
        this.astrologer_id = astrologer_id;
    }


    public String getClientTxn_amt() {
        return clientTxn_amt;
    }

    public void setClientTxn_amt(String clientTxn_amt) {
        this.clientTxn_amt = clientTxn_amt;
    }


    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getTransaction_amt() {
        return transaction_amt;
    }

    public void setTransaction_amt(String transaction_amt) {
        this.transaction_amt = transaction_amt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public String getPayOrderId() {
        return payOrderId;
    }

    public void setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
    }

    public String getPayReceipt() {
        return payReceipt;
    }

    public void setPayReceipt(String payReceipt) {
        this.payReceipt = payReceipt;
    }


    @SerializedName("product_order_id")
    private ProductOrderId productOrderId;

    @SerializedName("service_req_id")
    private ServiceReqId serviceReqId;

    @SerializedName("year")
    private int year;

    @SerializedName("payment_status")
    private String paymentStatus;



    @SerializedName("products")
    private List<ProductsItem> products;

    @SerializedName("consumer_id")
    private ConsumerId consumerId;


    @SerializedName("payment_type")
    private String paymentType;


    @SerializedName("pay_type")
    private String pay_type;

    @SerializedName("month")
    private int month;

    @SerializedName("_id")
    private String id;


    public ProductOrderId getProductOrderId(){
        return productOrderId;
    }

    public ServiceReqId getServiceReqId(){
        return serviceReqId;
    }

    public int getYear(){
        return year;
    }

    public String getPaymentStatus(){
        return paymentStatus;
    }

    public List<ProductsItem> getProducts(){
        return products;
    }

    public ConsumerId getConsumerId(){
        return consumerId;
    }


    public String getPaymentType(){
        return paymentType;
    }

    public int getMonth(){
        return month;
    }

    public String getId(){
        return id;
    }


    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }
}
