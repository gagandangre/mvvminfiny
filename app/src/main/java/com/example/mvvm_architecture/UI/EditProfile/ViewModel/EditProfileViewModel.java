package com.example.mvvm_architecture.UI.EditProfile.ViewModel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvm_architecture.UI.EditProfile.Repository.EditProfileRepo;
import com.example.mvvm_architecture.UI.Models.UserModel;
import com.example.mvvm_architecture.Utils.CommonUtils;

public class EditProfileViewModel extends ViewModel {

    EditProfileRepo editProfileRepo;
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private MutableLiveData<String> toastMessageObserver = new MutableLiveData();


    LiveData<UserModel> userModelLiveData;

    public EditProfileViewModel( ) {
        init();
    }

    private void init() {

        editProfileRepo = new EditProfileRepo();
        userModelLiveData = editProfileRepo.getUserModelMutableLiveData();

    }

    public  void editProfileApi(String firstName, String email, String phoneNumber, String imageString){
        isLoading.setValue(true);

        if(isValid(firstName,email,phoneNumber)){
            editProfileRepo.editProfileApi(firstName,email,phoneNumber,imageString);
        }

    }

    private boolean isValid(String firstName, String email, String phoneNumber) {

        boolean isValid = true;

        Log.i(getClass().getName(),"email--->"+email);

        if(firstName.trim().isEmpty()){
            isValid = false;
            toastMessageObserver.setValue("Please enter your name");
        }else if(!CommonUtils.isValidEmail(email.trim())){
            isValid = false;
            toastMessageObserver.setValue("Please enter valid email");
        }else if(phoneNumber.trim().isEmpty()){
            isValid = false;
            toastMessageObserver.setValue("Please enter your phone no.");
        }else if(phoneNumber.trim().length()<10){
            isValid = false;
            toastMessageObserver.setValue("Please enter 10 digit phone no.");
        }

        Log.i(getClass().getName(),"isValid--->"+isValid);

        return isValid;
    }

    public LiveData<String> getToastObserver(){
        return toastMessageObserver;
    }


    public MutableLiveData<Boolean> getIsLoading() {
        return editProfileRepo.getProgressBarLoadingState();
    }

    public LiveData<UserModel> getUserModelLiveData() {
        return userModelLiveData;
    }
}
