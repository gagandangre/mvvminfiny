package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("token")
    @Expose
    private String token;

    public UserDataModel getData() {
        return data;
    }

    public void setData(UserDataModel data) {
        this.data = data;
    }

    @SerializedName("data")
    @Expose
    private UserDataModel data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
