package com.example.mvvm_architecture.UI.PaginationPager3Library.View.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.example.mvvm_architecture.UI.Models.WalletBalanceData;

import org.jetbrains.annotations.NotNull;

public  class WalletBalanceDataCompare extends DiffUtil.ItemCallback<WalletBalanceData> {
    @Override
    public boolean areItemsTheSame(@NonNull @NotNull WalletBalanceData oldItem, @NonNull @NotNull WalletBalanceData newItem) {
        return oldItem.getId().equals(newItem.getId());
    }

    @Override
    public boolean areContentsTheSame(@NonNull @NotNull WalletBalanceData oldItem, @NonNull @NotNull WalletBalanceData newItem) {
        return oldItem.getId().equals(newItem.getId());
    }
}
