package com.example.mvvm_architecture.UI.RoomDB.view;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.RoomDB.viemodel.FormViewModel;
import com.example.mvvm_architecture.databinding.FragmentFormBinding;
import com.example.mvvm_architecture.databinding.FragmentHomeBinding;

import java.util.List;


public class FormFragment extends Fragment {


    private View view;
    private Context context;

    public FormFragment() {
        // Required empty public constructor
    }

    FragmentFormBinding fragmentHomeBinding;
    FormViewModel formViewModel;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        setInit();
        setListeners();
        return view;
    }

    private void setListeners() {

        fragmentHomeBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                formViewModel.insert(fragmentHomeBinding.edtFirstName.getText().toString(),
                        fragmentHomeBinding.edtEmail.getText().toString(),
                        fragmentHomeBinding.edtPhone.getText().toString());

            }
        });

    }

    private void setInit() {
        fragmentHomeBinding = FragmentFormBinding.inflate(getLayoutInflater());
        view = fragmentHomeBinding.getRoot();
        context = getActivity();

        formViewModel = new ViewModelProvider(this).get(FormViewModel.class);

    }
}