package com.example.mvvm_architecture.UI.Pagination.View;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.mvvm_architecture.UI.Pagination.Model.BlogDetailModel;
import com.example.mvvm_architecture.UI.Pagination.Model.BlogModel;
import com.example.mvvm_architecture.UI.Pagination.ViewModel.BlogViewModel;
import com.example.mvvm_architecture.Utils.CommonUtils;
import com.example.mvvm_architecture.databinding.ActivityPaginationBinding;

import java.util.ArrayList;
import java.util.List;

public class BlogActivity extends AppCompatActivity {

    ActivityPaginationBinding activityPaginationBinding;
    private BlogAdapter blogAdapter;
    private LinearLayoutManager linearLayoutManager;
    private Context context;
    private List<BlogDetailModel> blogDetailModelArrayList;
    private BlogViewModel blogViewModel;
    private ProgressDialog progressBar;
    int totalCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityPaginationBinding = ActivityPaginationBinding.inflate(getLayoutInflater());
        View view =  activityPaginationBinding.getRoot();
        setContentView(view);

        blogDetailModelArrayList = new ArrayList<>();
        context = this;

        setAdapter();

        blogViewModel = new ViewModelProvider(this).get(BlogViewModel.class);
        blogViewModel.fetchBlogsApi("token");
        blogViewModel.getBlogModelLiveData().observe(this, new Observer<BlogModel>() {
            @Override
            public void onChanged(BlogModel blogModel) {

                totalCount = blogModel.getTotalCount();
                Log.i(getClass().getName(),"totalCount--->"+totalCount);
                activityPaginationBinding.swiperefresh.setRefreshing(false);

                Log.i(getClass().getName(),"onChanged---->"+blogModel.getTotalCount());
                blogDetailModelArrayList.addAll(blogModel.getData());
                blogAdapter.notifyDataSetChanged();

            }
        });

        blogViewModel.getIsLoading().observe(this, (Boolean isLoading) -> {

            Log.i(getClass().getName(),"getIsLoading--->"+isLoading);
            if (isLoading){
                progressBar = CommonUtils.showProgressdialog(context);
            } else {
                CommonUtils.hideLoading(progressBar);
            }

        });

        setListeners();

    }

    private void setAdapter() {
        blogAdapter = new BlogAdapter(context,blogDetailModelArrayList);
        linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        activityPaginationBinding.recyclerBlog.setLayoutManager(linearLayoutManager);
        activityPaginationBinding.recyclerBlog.setAdapter(blogAdapter);

    }

    private void setListeners() {

        activityPaginationBinding.swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        /*currentPage=1;
                        getBlogsApi(currentPage);
                        swipeRefreshLayout.setRefreshing(false);*/
                        activityPaginationBinding.swiperefresh.setRefreshing(true);
                        blogViewModel.fetchBlogsApi("token");
                    }
                }
        );


        activityPaginationBinding.recyclerBlog.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    blogViewModel.isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                blogViewModel.currentItems = linearLayoutManager.getChildCount();
                blogViewModel.totalItems = linearLayoutManager.getItemCount();
                blogViewModel.scrollOutItems = linearLayoutManager.findFirstVisibleItemPosition();

                blogViewModel.recyclerScrollingEvent(blogDetailModelArrayList,dx,dy,totalCount);
            }
        });

    }
}