package com.example.mvvm_architecture.UI;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LifecycleObserver;

public class MvvmApplication extends Application implements LifecycleObserver {

    static  MvvmApplication mvvmApplicationInstance;


    public static MvvmApplication get() {
        Log.i("TwilioChatApplication--", "get------");
        if (mvvmApplicationInstance == null) {
            mvvmApplicationInstance = new MvvmApplication();
        }

        return mvvmApplicationInstance;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        MvvmApplication.mvvmApplicationInstance = this;

    }


}
