package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConsumerId {

	@SerializedName("groupId")
	private String groupId;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("is_chat_listing")
	private boolean isChatListing;

	@SerializedName("languages_spoken")
	private List<Object> languagesSpoken;

	@SerializedName("pay_payment_id")
	private String payPaymentId;

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("password")
	private String password;

	@SerializedName("is_deleted")
	private boolean isDeleted;

	@SerializedName("user_type")
	private String userType;

	@SerializedName("subscribed_consumers")
	private List<Object> subscribedConsumers;

	@SerializedName("subscribed_users")
	private List<Object> subscribedUsers;

	@SerializedName("__v")
	private int V;

	@SerializedName("refer_code_blocked")
	private boolean referCodeBlocked;

	@SerializedName("shipping_name")
	private String shippingName;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("specialities")
	private List<Object> specialities;

	@SerializedName("email")
	private String email;

	@SerializedName("isVerifiedPhone")
	private boolean isVerifiedPhone;

	@SerializedName("updatedAt")
	private String updatedAt;

	@SerializedName("isVerifiedEmail")
	private boolean isVerifiedEmail;

	@SerializedName("approval_status")
	private String approvalStatus;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("wallet_balance")
	private double walletBalance;

	@SerializedName("notification_count")
	private int notificationCount;

	@SerializedName("certifications")
	private List<Object> certifications;

	@SerializedName("pay_receipt")
	private String payReceipt;

	@SerializedName("shipping_number")
	private String shippingNumber;

	@SerializedName("pay_order_id")
	private String payOrderId;

	@SerializedName("astrologer_status")
	private String astrologerStatus;

	@SerializedName("is_blocked")
	private boolean isBlocked;

	@SerializedName("unique_name")
	private String uniqueName;



	@SerializedName("device_token")
	private List<String> deviceToken;

	@SerializedName("referral_code")
	private String referralCode;

	@SerializedName("_id")
	private String id;

	public String getGroupId(){
		return groupId;
	}


	public String getDeviceType(){
		return deviceType;
	}

	public boolean isIsChatListing(){
		return isChatListing;
	}

	public List<Object> getLanguagesSpoken(){
		return languagesSpoken;
	}

	public String getPayPaymentId(){
		return payPaymentId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getPassword(){
		return password;
	}

	public boolean isIsDeleted(){
		return isDeleted;
	}

	public String getUserType(){
		return userType;
	}

	public List<Object> getSubscribedConsumers(){
		return subscribedConsumers;
	}

	public List<Object> getSubscribedUsers(){
		return subscribedUsers;
	}

	public int getV(){
		return V;
	}

	public boolean isReferCodeBlocked(){
		return referCodeBlocked;
	}

	public String getShippingName(){
		return shippingName;
	}

	public String getFirstName(){
		return firstName;
	}

	public List<Object> getSpecialities(){
		return specialities;
	}

	public String getEmail(){
		return email;
	}

	public boolean isIsVerifiedPhone(){
		return isVerifiedPhone;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public boolean isIsVerifiedEmail(){
		return isVerifiedEmail;
	}

	public String getApprovalStatus(){
		return approvalStatus;
	}

	public String getMobile(){
		return mobile;
	}

	public double getWalletBalance(){
		return walletBalance;
	}

	public int getNotificationCount(){
		return notificationCount;
	}

	public List<Object> getCertifications(){
		return certifications;
	}

	public String getPayReceipt(){
		return payReceipt;
	}

	public String getShippingNumber(){
		return shippingNumber;
	}

	public String getPayOrderId(){
		return payOrderId;
	}

	public String getAstrologerStatus(){
		return astrologerStatus;
	}

	public boolean isIsBlocked(){
		return isBlocked;
	}

	public String getUniqueName(){
		return uniqueName;
	}


	public List<String> getDeviceToken(){
		return deviceToken;
	}

	public String getReferralCode(){
		return referralCode;
	}

	public String getId(){
		return id;
	}
}