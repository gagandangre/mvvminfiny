package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FromId implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("device_token")
    @Expose
    private List<Object> deviceToken = null;
    @SerializedName("wallet_balance")
    @Expose
    private Double walletBalance;
    @SerializedName("certificates")
    @Expose
    private List<Object> certificates = null;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("isVerifiedEmail")
    @Expose
    private Boolean isVerifiedEmail;
    @SerializedName("isVerifiedPhone")
    @Expose
    private Boolean isVerifiedPhone;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("experience_years")
    @Expose
    private Integer experienceYears;
    @SerializedName("unique_name")
    @Expose
    private String uniqueName;
    @SerializedName("assigned_services")
    @Expose
    private List<Object> assignedServices = null;
    @SerializedName("astro_sign")
    @Expose
    private String astroSign;

    @SerializedName("groupId")
    @Expose
    private String groupId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("certifications")
    @Expose
    private List<Object> certifications = null;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("is_blocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("ratings")
    @Expose
    private List<Object> ratings = null;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("notification_count")
    @Expose
    private Integer notificationCount;
    @SerializedName("subscribed_consumers")
    @Expose
    private List<Object> subscribedConsumers = null;
    @SerializedName("approval_status")
    @Expose
    private String approvalStatus;
    @SerializedName("refer_code_blocked")
    @Expose
    private Boolean referCodeBlocked;
    @SerializedName("is_listing")
    @Expose
    private Boolean isListing;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("pay_order_id")
    @Expose
    private String payOrderId;
    @SerializedName("pay_receipt")
    @Expose
    private String payReceipt;
    @SerializedName("profile_url")
    @Expose
    private String profileUrl;
    private final static long serialVersionUID = 8444225446574231028L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Object> getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(List<Object> deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(Double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public List<Object> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Object> certificates) {
        this.certificates = certificates;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsVerifiedEmail() {
        return isVerifiedEmail;
    }

    public void setIsVerifiedEmail(Boolean isVerifiedEmail) {
        this.isVerifiedEmail = isVerifiedEmail;
    }

    public Boolean getIsVerifiedPhone() {
        return isVerifiedPhone;
    }

    public void setIsVerifiedPhone(Boolean isVerifiedPhone) {
        this.isVerifiedPhone = isVerifiedPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getExperienceYears() {
        return experienceYears;
    }

    public void setExperienceYears(Integer experienceYears) {
        this.experienceYears = experienceYears;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public List<Object> getAssignedServices() {
        return assignedServices;
    }

    public void setAssignedServices(List<Object> assignedServices) {
        this.assignedServices = assignedServices;
    }

    public String getAstroSign() {
        return astroSign;
    }

    public void setAstroSign(String astroSign) {
        this.astroSign = astroSign;
    }



    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public List<Object> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<Object> certifications) {
        this.certifications = certifications;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }


    public List<Object> getRatings() {
        return ratings;
    }

    public void setRatings(List<Object> ratings) {
        this.ratings = ratings;
    }



    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    public List<Object> getSubscribedConsumers() {
        return subscribedConsumers;
    }

    public void setSubscribedConsumers(List<Object> subscribedConsumers) {
        this.subscribedConsumers = subscribedConsumers;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Boolean getReferCodeBlocked() {
        return referCodeBlocked;
    }

    public void setReferCodeBlocked(Boolean referCodeBlocked) {
        this.referCodeBlocked = referCodeBlocked;
    }

    public Boolean getIsListing() {
        return isListing;
    }

    public void setIsListing(Boolean isListing) {
        this.isListing = isListing;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }



    public String getPayOrderId() {
        return payOrderId;
    }

    public void setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
    }

    public String getPayReceipt() {
        return payReceipt;
    }

    public void setPayReceipt(String payReceipt) {
        this.payReceipt = payReceipt;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

}
