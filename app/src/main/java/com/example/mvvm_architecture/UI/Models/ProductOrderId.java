package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductOrderId {

	@SerializedName("is_viewed")
	private boolean isViewed;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("payment_status")
	private String paymentStatus;

	@SerializedName("pay_receipt")
	private String payReceipt;

	@SerializedName("pay_order_id")
	private String payOrderId;

	@SerializedName("pay_payment_id")
	private String payPaymentId;

	@SerializedName("products")
	private Products products;

	@SerializedName("consumer_id")
	private String consumerId;

	@SerializedName("order_status")
	private List<OrderStatusItem> orderStatus;

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("is_deleted")
	private boolean isDeleted;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("total_amount")
	private double totalAmount;

	@SerializedName("__v")
	private int V;

	@SerializedName("current_status")
	private String currentStatus;

	@SerializedName("_id")
	private String id;

	@SerializedName("updatedAt")
	private String updatedAt;

	public boolean isIsViewed(){
		return isViewed;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public String getPaymentStatus(){
		return paymentStatus;
	}

	public String getPayReceipt(){
		return payReceipt;
	}

	public String getPayOrderId(){
		return payOrderId;
	}

	public String getPayPaymentId(){
		return payPaymentId;
	}

	public Products getProducts(){
		return products;
	}

	public String getConsumerId(){
		return consumerId;
	}

	public List<OrderStatusItem> getOrderStatus(){
		return orderStatus;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public boolean isIsDeleted(){
		return isDeleted;
	}

	public String getUserId(){
		return userId;
	}

	public double getTotalAmount(){
		return totalAmount;
	}

	public int getV(){
		return V;
	}

	public String getCurrentStatus(){
		return currentStatus;
	}

	public String getId(){
		return id;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}
}