package com.example.mvvm_architecture.UI.Pagination.Repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mvvm_architecture.Apis.UserApi;
import com.example.mvvm_architecture.UI.Pagination.Model.BlogModel;
import com.example.mvvm_architecture.Utils.CommonUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class BlogRepository {

    UserApi userApi;
    private MutableLiveData<BlogModel> blogModelMutableLiveData;
    private MutableLiveData<Boolean> progressBarState;


    public BlogRepository() {
        progressBarState = new MutableLiveData<>();

        blogModelMutableLiveData = new MutableLiveData<>();
        userApi = CommonUtils.getApiInterface();

    }

    public void fetchBlogsApi(String token, int perPage, int currentPage) {
        progressBarState.postValue(true);
        Log.i(getClass().getName(),"fetchBlogsApi---->"+token);
        Log.i(getClass().getName(),"perPage---->"+perPage);
        Log.i(getClass().getName(),"currentPage---->"+currentPage);

        userApi.getBlogList("token",perPage,currentPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::getBlogsApiSuccess, this::getBlogsApiFailure);

    }

    private void getBlogsApiFailure(Throwable throwable) {
        progressBarState.postValue(false);
    }

    private void getBlogsApiSuccess(BlogModel blogModel) {
        blogModelMutableLiveData.postValue(blogModel);
        progressBarState.postValue(false);
    }

    public LiveData<BlogModel> getBlogModelMutableLiveData() {
        return blogModelMutableLiveData;
    }

    public MutableLiveData<Boolean> getProgressBarLoadingState() {
        Log.i(getClass().getName(),"getProgressBarLoadingState--->");
        return progressBarState;
    }
}
