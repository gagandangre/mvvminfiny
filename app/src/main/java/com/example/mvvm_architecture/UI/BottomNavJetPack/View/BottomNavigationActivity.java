package com.example.mvvm_architecture.UI.BottomNavJetPack.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.BottomNavJetPack.View.Home.viewmodel.HomeViewModel;
import com.example.mvvm_architecture.UI.MvvmApplication;
import com.example.mvvm_architecture.databinding.ActivityBottomNavigationBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class BottomNavigationActivity extends AppCompatActivity {

    ActivityBottomNavigationBinding activityBottomNavigationBinding;
    public static   HomeViewModel homeViewModel;
    Context context;
    private NavController navController;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setInit();
        setListeners();
    }

    private void setListeners() {



    }

    private void setInit() {
        activityBottomNavigationBinding = ActivityBottomNavigationBinding.inflate(getLayoutInflater());
        View view = activityBottomNavigationBinding.getRoot();
        setContentView(view);
        context=this;

        Log.i(getClass().getName(),"homeViewModel-setInit-->");
        homeViewModel = new ViewModelProvider(this,new SavedStateViewModelFactory(MvvmApplication.get(),this)).get(HomeViewModel.class);
        Log.i(getClass().getName(),"homeViewModel--->"+homeViewModel);

        navController = Navigation.findNavController(this, R.id.activity_main_nav_host_fragment);
        BottomNavigationView bottomNavigationView = findViewById(R.id.activity_main_bottom_navigation_view);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);



    }


    @Override
    public void onBackPressed() {
        Log.i(getClass().getName(),"getId--->"+doubleBackToExitPressedOnce);
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
