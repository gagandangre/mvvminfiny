package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserDataModel implements Serializable {


    /*@SerializedName("background_color")
    @Expose
    private BackgroundColor backgroundColor;*/
    @SerializedName("wallet_balance")
    @Expose
    private Double walletBalance;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;

    @SerializedName("is_chat")
    @Expose
    private Boolean isChat;

    public Boolean getChat() {
        return isChat;
    }

    public void setChat(Boolean chat) {
        isChat = chat;
    }

    public Boolean getVideo() {
        return isVideo;
    }

    public void setVideo(Boolean video) {
        isVideo = video;
    }

    public Boolean getAudio() {
        return isAudio;
    }

    public void setAudio(Boolean audio) {
        isAudio = audio;
    }

    public Boolean getReport() {
        return isReport;
    }

    public void setReport(Boolean report) {
        isReport = report;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @SerializedName("is_video")
    @Expose
    private Boolean isVideo;

    @SerializedName("is_audio")
    @Expose
    private Boolean isAudio;

    @SerializedName("is_report")
    @Expose
    private Boolean isReport;

    @SerializedName("info")
    @Expose
    private String info;

    @SerializedName("birth_place")
    @Expose
    private String birthPlace;

    @SerializedName("birth_location")
    @Expose
    private ArrayList<String> birthLocation;


    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    @SerializedName("profile_url")
    @Expose
    private String profileUrl;

    @SerializedName("astrologer_status")
    @Expose
    private String astrologerStatus;

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public ArrayList<String> getBirthLocation() {
        return birthLocation;
    }

    public void setBirthLocation(ArrayList<String> birthLocation) {
        this.birthLocation = birthLocation;
    }

    public String getAstrologerStatus() {
        return astrologerStatus;
    }

    public void setAstrologerStatus(String astrologerStatus) {
        this.astrologerStatus = astrologerStatus;
    }


    @SerializedName("is_blocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("isVerifiedEmail")
    @Expose
    private Boolean isVerifiedEmail;
    @SerializedName("isVerifiedPhone")
    @Expose
    private Boolean isVerifiedPhone;
    @SerializedName("ratings")
    @Expose
    private List<Object> ratings = null;
    @SerializedName("notification_count")
    @Expose
    private Integer notificationCount;
    @SerializedName("subscribed_consumers")
    @Expose
    private List<Object> subscribedConsumers = null;
    @SerializedName("refer_code_blocked")
    @Expose
    private Boolean referCodeBlocked;

    public String getReferral_code() {
        return referral_code;
    }

    public void setReferral_code(String referral_code) {
        this.referral_code = referral_code;
    }

    @SerializedName("referral_code")
    @Expose
    private String referral_code;

    @SerializedName("approval_status")
    @Expose
    private String approvalStatus;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("mobile")
    @Expose
    private String mobile;


    @SerializedName("unique_name")
    @Expose
    private String uniqueName;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;

    @SerializedName("birth_time")
    @Expose
    private String birth_time;

    public String getBirth_time() {
        return birth_time;
    }

    public void setBirth_time(String birth_time) {
        this.birth_time = birth_time;
    }


    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("certifications")
    @Expose
    private List<Object> certifications = null;
    @SerializedName("assigned_services")
    @Expose
    private List<Object> assignedServices = null;


    @SerializedName("client_report_rate")
    @Expose
    private String client_report_rate;

    @SerializedName("client_audio_rate")
    @Expose
    private String client_audio_rate;

    @SerializedName("client_video_rate")
    @Expose
    private String client_video_rate;

    @SerializedName("client_chat_rate")
    @Expose
    private String client_chat_rate;

    @SerializedName("report_rate")
    @Expose
    private String report_rate;

    @SerializedName("audio_rate")
    @Expose
    private String audio_rate;

    @SerializedName("video_rate")
    @Expose
    private String video_rate;

    @SerializedName("chat_rate")
    @Expose
    private String chat_rate;

    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("groupId")
    @Expose
    private String groupId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("is_listing")
    @Expose
    private Boolean isListing;
    @SerializedName("pay_order_id")
    @Expose
    private String payOrderId;
    @SerializedName("pay_receipt")
    @Expose
    private String payReceipt;


    @SerializedName("experience_years")
    @Expose
    private String experienceYears;

    @SerializedName("total_rating")
    @Expose
    private String total_rating;

    @SerializedName("five_stars")
    @Expose
    private Integer fiveStars;
    @SerializedName("four_stars")
    @Expose
    private Integer fourStars;
    @SerializedName("three_stars")
    @Expose
    private Integer threeStars;
    @SerializedName("two_stars")
    @Expose
    private Integer twoStars;
    @SerializedName("one_stars")
    @Expose
    private Integer oneStars;

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    @SerializedName("avg_rating")
    @Expose
    private String avgRating;

    @SerializedName("rating_count")
    @Expose
    private String ratingCount;

    @SerializedName("shipping_name")
    @Expose
    private String shippingName;

    @SerializedName("shipping_number")
    @Expose
    private String shippingNumber;



    public String getClient_report_rate() {
        return client_report_rate;
    }

    public void setClient_report_rate(String client_report_rate) {
        this.client_report_rate = client_report_rate;
    }

    public String getClient_audio_rate() {
        return client_audio_rate;
    }

    public void setClient_audio_rate(String client_audio_rate) {
        this.client_audio_rate = client_audio_rate;
    }

    public String getClient_video_rate() {
        return client_video_rate;
    }

    public void setClient_video_rate(String client_video_rate) {
        this.client_video_rate = client_video_rate;
    }

    public String getClient_chat_rate() {
        return client_chat_rate;
    }

    public void setClient_chat_rate(String client_chat_rate) {
        this.client_chat_rate = client_chat_rate;
    }

    public String getReport_rate() {
        return report_rate;
    }

    public void setReport_rate(String report_rate) {
        this.report_rate = report_rate;
    }

    public String getAudio_rate() {
        return audio_rate;
    }

    public void setAudio_rate(String audio_rate) {
        this.audio_rate = audio_rate;
    }

    public String getVideo_rate() {
        return video_rate;
    }

    public void setVideo_rate(String video_rate) {
        this.video_rate = video_rate;
    }

    public String getChat_rate() {
        return chat_rate;
    }

    public void setChat_rate(String chat_rate) {
        this.chat_rate = chat_rate;
    }

/*
    public BackgroundColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(BackgroundColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }*/

    public Double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(Double walletBalance) {
        this.walletBalance = walletBalance;
    }


    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public String getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(String ratingCount) {
        this.ratingCount = ratingCount;
    }

    public String getExperienceYears() {
        return experienceYears;
    }

    public void setExperienceYears(String experienceYears) {
        this.experienceYears = experienceYears;
    }

    public String getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(String total_rating) {
        this.total_rating = total_rating;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Boolean getIsVerifiedEmail() {
        return isVerifiedEmail;
    }

    public void setIsVerifiedEmail(Boolean isVerifiedEmail) {
        this.isVerifiedEmail = isVerifiedEmail;
    }

    public Boolean getIsVerifiedPhone() {
        return isVerifiedPhone;
    }

    public void setIsVerifiedPhone(Boolean isVerifiedPhone) {
        this.isVerifiedPhone = isVerifiedPhone;
    }

    public List<Object> getRatings() {
        return ratings;
    }

    public void setRatings(List<Object> ratings) {
        this.ratings = ratings;
    }

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    public List<Object> getSubscribedConsumers() {
        return subscribedConsumers;
    }

    public void setSubscribedConsumers(List<Object> subscribedConsumers) {
        this.subscribedConsumers = subscribedConsumers;
    }

    public Integer getFiveStars() {
        return fiveStars;
    }

    public void setFiveStars(Integer fiveStars) {
        this.fiveStars = fiveStars;
    }

    public Integer getFourStars() {
        return fourStars;
    }

    public void setFourStars(Integer fourStars) {
        this.fourStars = fourStars;
    }

    public Integer getThreeStars() {
        return threeStars;
    }

    public void setThreeStars(Integer threeStars) {
        this.threeStars = threeStars;
    }

    public Integer getTwoStars() {
        return twoStars;
    }

    public void setTwoStars(Integer twoStars) {
        this.twoStars = twoStars;
    }

    public Integer getOneStars() {
        return oneStars;
    }

    public void setOneStars(Integer oneStars) {
        this.oneStars = oneStars;
    }

    public Boolean getReferCodeBlocked() {
        return referCodeBlocked;
    }

    public void setReferCodeBlocked(Boolean referCodeBlocked) {
        this.referCodeBlocked = referCodeBlocked;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<Object> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<Object> certifications) {
        this.certifications = certifications;
    }

    public List<Object> getAssignedServices() {
        return assignedServices;
    }

    public void setAssignedServices(List<Object> assignedServices) {
        this.assignedServices = assignedServices;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsListing() {
        return isListing;
    }

    public void setIsListing(Boolean isListing) {
        this.isListing = isListing;
    }

    public String getPayOrderId() {
        return payOrderId;
    }

    public void setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
    }

    public String getPayReceipt() {
        return payReceipt;
    }

    public void setPayReceipt(String payReceipt) {
        this.payReceipt = payReceipt;
    }

    public String getShippingName() {
        return shippingName;
    }

    public String getShippingNumber() {
        return shippingNumber;
    }


    @Override
    public String toString() {
        return "UserDataModel{" +
                "walletBalance=" + walletBalance +
                ", isDeleted=" + isDeleted +
                ", isChat=" + isChat +
                ", isVideo=" + isVideo +
                ", isAudio=" + isAudio +
                ", isReport=" + isReport +
                ", info='" + info + '\'' +
                ", birthPlace='" + birthPlace + '\'' +
                ", birthLocation=" + birthLocation +
                ", profileUrl='" + profileUrl + '\'' +
                ", astrologerStatus='" + astrologerStatus + '\'' +
                ", isBlocked=" + isBlocked +
                ", isVerifiedEmail=" + isVerifiedEmail +
                ", isVerifiedPhone=" + isVerifiedPhone +
                ", ratings=" + ratings +
                ", notificationCount=" + notificationCount +
                ", subscribedConsumers=" + subscribedConsumers +
                ", referCodeBlocked=" + referCodeBlocked +
                ", referral_code='" + referral_code + '\'' +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", userType='" + userType + '\'' +
                ", mobile='" + mobile + '\'' +
                ", uniqueName='" + uniqueName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", gender='" + gender + '\'' +
                ", certifications=" + certifications +
                ", assignedServices=" + assignedServices +
                ", client_report_rate='" + client_report_rate + '\'' +
                ", client_audio_rate='" + client_audio_rate + '\'' +
                ", client_video_rate='" + client_video_rate + '\'' +
                ", client_chat_rate='" + client_chat_rate + '\'' +
                ", report_rate='" + report_rate + '\'' +
                ", audio_rate='" + audio_rate + '\'' +
                ", video_rate='" + video_rate + '\'' +
                ", chat_rate='" + chat_rate + '\'' +
                ", password='" + password + '\'' +
                ", groupId='" + groupId + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", isListing=" + isListing +
                ", payOrderId='" + payOrderId + '\'' +
                ", payReceipt='" + payReceipt + '\'' +
                ", experienceYears='" + experienceYears + '\'' +
                ", total_rating='" + total_rating + '\'' +
                ", fiveStars=" + fiveStars +
                ", fourStars=" + fourStars +
                ", threeStars=" + threeStars +
                ", twoStars=" + twoStars +
                ", oneStars=" + oneStars +
                ", avgRating='" + avgRating + '\'' +
                ", ratingCount='" + ratingCount + '\'' +
                '}';
    }
}
