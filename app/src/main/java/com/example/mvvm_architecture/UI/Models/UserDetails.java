package com.example.mvvm_architecture.UI.Models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "forms",indices = @Index(value = {"room_id"}, unique = true))
public class UserDetails implements Serializable {

    @PrimaryKey
    @NonNull
    @SerializedName("room_id")
    @Expose
    private Integer room_id;

    @NonNull
    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(@NonNull Integer room_id) {
        this.room_id = room_id;
    }

    @SerializedName("first_name")
    String firstName;

    @SerializedName("last_name")
    String last_name;

    @SerializedName("email")
    String email;

    @SerializedName("user_type")
    String user_type;


    public String getProfile_url() {
        return profile_url;
    }

    public void setProfile_url(String profile_url) {
        this.profile_url = profile_url;
    }

    @SerializedName("profile_url")
    String profile_url;


    @SerializedName("mobile")
    String mobile;



    @SerializedName("device_token")
    String deviceToken;

    @SerializedName("date_of_birth")
    String date_of_birth;

    @SerializedName("password")
    String password;

    @SerializedName("google_id")
    String google_id;

    @SerializedName("facebook_id")
    String facebook_id;

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGoogle_id() {
        return google_id;
    }

    public void setGoogle_id(String google_id) {
        this.google_id = google_id;
    }

    public String getFacebook_id() {
        return facebook_id;
    }

    public void setFacebook_id(String facebook_id) {
        this.facebook_id = facebook_id;
    }
}
