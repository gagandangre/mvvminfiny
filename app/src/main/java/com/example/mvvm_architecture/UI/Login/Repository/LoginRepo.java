package com.example.mvvm_architecture.UI.Login.Repository;

import androidx.lifecycle.MutableLiveData;

import com.example.mvvm_architecture.Apis.UserApi;
import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.Models.UserModel;
import com.example.mvvm_architecture.Utils.CommonUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LoginRepo {

    MutableLiveData<UserModel> userDataModelMutableLiveData;
    MutableLiveData<Boolean> booleanMutableLiveData;
    UserApi userApi;


    public LoginRepo() {

        userDataModelMutableLiveData = new MutableLiveData<>();
        booleanMutableLiveData = new MutableLiveData<>();

    }

    public void loginApi(String phoneNumber, String password){
        booleanMutableLiveData.setValue(true);
        userApi = CommonUtils.getApiInterface();

        UserDetails userDetails = new UserDetails();
        userDetails.setMobile(phoneNumber);
        userDetails.setPassword(password);
        userDetails.setDeviceToken(CommonUtils.token);
        userDetails.setUser_type("consumer");


        userApi.userLogin(userDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::loginSuccess, this::loginFailure);

    }

    private void loginFailure(Throwable throwable) {
//        CommonUtils.showError(throwable,context);
        booleanMutableLiveData.setValue(false);

    }

    private void loginSuccess(UserModel userModel) {
        booleanMutableLiveData.setValue(false);
        userDataModelMutableLiveData.postValue(userModel);
    }

    public MutableLiveData<UserModel> getUserDataModelMutableLiveData() {
        return userDataModelMutableLiveData;
    }

    public MutableLiveData<Boolean> getBooleanMutableLiveData() {
        return booleanMutableLiveData;
    }
}
