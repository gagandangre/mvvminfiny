package com.example.mvvm_architecture.Utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;


import com.example.mvvm_architecture.R;

import java.util.Calendar;

public class MonthYearPickerDialog extends DialogFragment {

    private static final int MAX_YEAR = 2099;
    private DatePickerDialog.OnDateSetListener listener;
    String btnString = "Download";
    int monthC;
    int yearC;
    int filterApplied;


    String[] mntData = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    public void setListener(DatePickerDialog.OnDateSetListener listener, String postvBtnstg, int monthC, int yearC, int i2) {
        this.listener = listener;
        btnString = postvBtnstg;
        this.monthC = monthC;
        this.yearC = yearC;
        filterApplied = i2;

    }

    public void setListener(DatePickerDialog.OnDateSetListener listener, String postvBtnstg) {
        this.listener = listener;
        btnString = postvBtnstg;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        Calendar cal = Calendar.getInstance();

        View dialog = inflater.inflate(R.layout.month_year_picker_dialog, null);
        final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
        final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);


        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);
        monthPicker.setDisplayedValues(mntData);

//        int year = cal.get(Calendar.YEAR);
        yearPicker.setMinValue(2019);
        yearPicker.setMaxValue(MAX_YEAR);
//        monthPicker.setValue(cal.get(Calendar.MONTH) + 1);
        if (filterApplied == 0) {

            monthPicker.setValue(Calendar.getInstance().get(Calendar.MONTH) + 1);
            yearPicker.setValue(Calendar.getInstance().get(Calendar.YEAR));


        } else if (filterApplied == 1) {
            monthPicker.setValue(monthC + 1);
            yearPicker.setValue(yearC);
        }
        Log.i(getClass().getName(), "monthC>>>>" + monthC + "filterApplied==>" + filterApplied);


        Log.i(getClass().getName(), "yearC>>>>" + yearC);

        /*
        builder.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(COLOR_I_WANT);
            }
        });
        */
        if (btnString.equals("Download")) {

            builder.setView(dialog)
                    // Add action buttons
                    .setPositiveButton(btnString, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            listener.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), filterApplied);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
//                            listener.onDateSet(null, 0, 0, 0);
                            MonthYearPickerDialog.this.getDialog().cancel();
                            dialog.dismiss();
                        }
                    });

        } else {
            builder.setView(dialog)
                    // Add action buttons
                    .setPositiveButton(btnString, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            filterApplied = 1;
                            Log.i("createDialo", " Download  filterApplied" + filterApplied);
                            listener.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), filterApplied);
                        }
                    })
                    .setNegativeButton("Clear", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            monthC =Calendar.getInstance().get(Calendar.MONTH)+1;
                            yearC = Calendar.getInstance().get(Calendar.YEAR);
                            listener.onDateSet(null, yearC, monthC, 0);
                            MonthYearPickerDialog.this.getDialog().cancel();
                        }
                    });
        }
        AlertDialog monthlyIncomedailog = builder.create();
        monthlyIncomedailog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                monthlyIncomedailog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getContext().getResources().getColor(R.color.text_grey));
            }
        });
        return monthlyIncomedailog;
    }
}