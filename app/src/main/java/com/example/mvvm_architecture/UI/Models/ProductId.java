package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.SerializedName;

public class ProductId {

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("is_deleted")
	private boolean isDeleted;

	@SerializedName("rate")
	private double rate;

	@SerializedName("image_url")
	private String imageUrl;

	@SerializedName("__v")
	private int V;

	@SerializedName("is_hidden")
	private boolean isHidden;

	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("_id")
	private String id;

	@SerializedName("updatedAt")
	private String updatedAt;

	public String getCreatedAt(){
		return createdAt;
	}

	public boolean isIsDeleted(){
		return isDeleted;
	}

	public double getRate(){
		return rate;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public int getV(){
		return V;
	}

	public boolean isIsHidden(){
		return isHidden;
	}

	public String getName(){
		return name;
	}

	public String getDescription(){
		return description;
	}

	public String getId(){
		return id;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}
}