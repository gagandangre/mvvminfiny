package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RatingUserDataModel implements Serializable {


    /*@SerializedName("background_color")
    @Expose
    private BackgroundColor backgroundColor;*/
    @SerializedName("wallet_balance")
    @Expose
    private Double walletBalance;

    @SerializedName("content")
    @Expose
    private String content;



    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;

    @SerializedName("is_chat")
    @Expose
    private Boolean isChat;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getChat() {
        return isChat;
    }

    public void setChat(Boolean chat) {
        isChat = chat;
    }

    public Boolean getVideo() {
        return isVideo;
    }

    public void setVideo(Boolean video) {
        isVideo = video;
    }

    public Boolean getAudio() {
        return isAudio;
    }

    public void setAudio(Boolean audio) {
        isAudio = audio;
    }

    public Boolean getReport() {
        return isReport;
    }

    public void setReport(Boolean report) {
        isReport = report;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @SerializedName("is_video")
    @Expose
    private Boolean isVideo;

    @SerializedName("is_audio")
    @Expose
    private Boolean isAudio;

    @SerializedName("is_report")
    @Expose
    private Boolean isReport;

    @SerializedName("info")
    @Expose
    private String info;

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    @SerializedName("profile_url")
    @Expose
    private String profileUrl;


    @SerializedName("is_blocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("isVerifiedEmail")
    @Expose
    private Boolean isVerifiedEmail;
    @SerializedName("isVerifiedPhone")
    @Expose
    private Boolean isVerifiedPhone;
    @SerializedName("ratings")
    @Expose
    private List<Object> ratings = null;
    @SerializedName("notification_count")
    @Expose
    private Integer notificationCount;
    @SerializedName("subscribed_consumers")
    @Expose
    private List<Object> subscribedConsumers = null;
    @SerializedName("refer_code_blocked")
    @Expose
    private Boolean referCodeBlocked;
    @SerializedName("approval_status")
    @Expose
    private String approvalStatus;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("mobile")
    @Expose
    private String mobile;

    @SerializedName("user_ratings")
    @Expose
    List<UserRating> userrating;

    @SerializedName("unique_name")
    @Expose
    private String uniqueName;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("astro_sign")
    @Expose
    private String astroSign;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("certifications")
    @Expose
    private List<Object> certifications = null;
    @SerializedName("assigned_services")
    @Expose
    private List<Object> assignedServices = null;


    @SerializedName("client_report_rate")
    @Expose
    private String client_report_rate;

    @SerializedName("client_audio_rate")
    @Expose
    private String client_audio_rate;

    @SerializedName("client_video_rate")
    @Expose
    private String client_video_rate;

    @SerializedName("client_chat_rate")
    @Expose
    private String client_chat_rate;

    @SerializedName("report_rate")
    @Expose
    private String report_rate;

    @SerializedName("audio_rate")
    @Expose
    private String audio_rate;

    @SerializedName("video_rate")
    @Expose
    private String video_rate;

    @SerializedName("chat_rate")
    @Expose
    private String chat_rate;

    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("groupId")
    @Expose
    private String groupId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("is_listing")
    @Expose
    private Boolean isListing;
    @SerializedName("pay_order_id")
    @Expose
    private String payOrderId;
    @SerializedName("pay_receipt")
    @Expose
    private String payReceipt;


    @SerializedName("experience_years")
    @Expose
    private String experienceYears;

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    @SerializedName("avg_rating")
    @Expose
    private String avgRating;

    @SerializedName("total_rating")
    @Expose
    private String total_rating;

    @SerializedName("five_stars")
    @Expose
    private Integer fiveStars;
    @SerializedName("four_stars")
    @Expose
    private Integer fourStars;
    @SerializedName("three_stars")
    @Expose
    private Integer threeStars;
    @SerializedName("two_stars")
    @Expose
    private Integer twoStars;
    @SerializedName("one_stars")
    @Expose
    private Integer oneStars;


    @SerializedName("rating_count")
    @Expose
    private String ratingCount;

    public List<UserRating> getUserrating() {
        return userrating;
    }

    public void setUserrating(List<UserRating> userrating) {
        this.userrating = userrating;
    }

    public String getClient_report_rate() {
        return client_report_rate;
    }

    public void setClient_report_rate(String client_report_rate) {
        this.client_report_rate = client_report_rate;
    }

    public String getClient_audio_rate() {
        return client_audio_rate;
    }

    public void setClient_audio_rate(String client_audio_rate) {
        this.client_audio_rate = client_audio_rate;
    }

    public String getClient_video_rate() {
        return client_video_rate;
    }

    public void setClient_video_rate(String client_video_rate) {
        this.client_video_rate = client_video_rate;
    }

    public String getClient_chat_rate() {
        return client_chat_rate;
    }

    public void setClient_chat_rate(String client_chat_rate) {
        this.client_chat_rate = client_chat_rate;
    }

    public String getReport_rate() {
        return report_rate;
    }

    public void setReport_rate(String report_rate) {
        this.report_rate = report_rate;
    }

    public String getAudio_rate() {
        return audio_rate;
    }

    public void setAudio_rate(String audio_rate) {
        this.audio_rate = audio_rate;
    }

    public String getVideo_rate() {
        return video_rate;
    }

    public void setVideo_rate(String video_rate) {
        this.video_rate = video_rate;
    }

    public String getChat_rate() {
        return chat_rate;
    }

    public void setChat_rate(String chat_rate) {
        this.chat_rate = chat_rate;
    }

/*
    public BackgroundColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(BackgroundColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }*/

    public Double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(Double walletBalance) {
        this.walletBalance = walletBalance;
    }


    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public String getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(String ratingCount) {
        this.ratingCount = ratingCount;
    }

    public String getExperienceYears() {
        return experienceYears;
    }

    public void setExperienceYears(String experienceYears) {
        this.experienceYears = experienceYears;
    }

    public String getTotal_rating() {
        return total_rating;
    }

    public void setTotal_rating(String total_rating) {
        this.total_rating = total_rating;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public Boolean getIsVerifiedEmail() {
        return isVerifiedEmail;
    }

    public void setIsVerifiedEmail(Boolean isVerifiedEmail) {
        this.isVerifiedEmail = isVerifiedEmail;
    }

    public Boolean getIsVerifiedPhone() {
        return isVerifiedPhone;
    }

    public void setIsVerifiedPhone(Boolean isVerifiedPhone) {
        this.isVerifiedPhone = isVerifiedPhone;
    }

    public List<Object> getRatings() {
        return ratings;
    }

    public void setRatings(List<Object> ratings) {
        this.ratings = ratings;
    }

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    public List<Object> getSubscribedConsumers() {
        return subscribedConsumers;
    }

    public void setSubscribedConsumers(List<Object> subscribedConsumers) {
        this.subscribedConsumers = subscribedConsumers;
    }

    public Integer getFiveStars() {
        return fiveStars;
    }

    public void setFiveStars(Integer fiveStars) {
        this.fiveStars = fiveStars;
    }

    public Integer getFourStars() {
        return fourStars;
    }

    public void setFourStars(Integer fourStars) {
        this.fourStars = fourStars;
    }

    public Integer getThreeStars() {
        return threeStars;
    }

    public void setThreeStars(Integer threeStars) {
        this.threeStars = threeStars;
    }

    public Integer getTwoStars() {
        return twoStars;
    }

    public void setTwoStars(Integer twoStars) {
        this.twoStars = twoStars;
    }

    public Integer getOneStars() {
        return oneStars;
    }

    public void setOneStars(Integer oneStars) {
        this.oneStars = oneStars;
    }

    public Boolean getReferCodeBlocked() {
        return referCodeBlocked;
    }

    public void setReferCodeBlocked(Boolean referCodeBlocked) {
        this.referCodeBlocked = referCodeBlocked;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAstroSign() {
        return astroSign;
    }

    public void setAstroSign(String astroSign) {
        this.astroSign = astroSign;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<Object> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<Object> certifications) {
        this.certifications = certifications;
    }

    public List<Object> getAssignedServices() {
        return assignedServices;
    }

    public void setAssignedServices(List<Object> assignedServices) {
        this.assignedServices = assignedServices;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsListing() {
        return isListing;
    }

    public void setIsListing(Boolean isListing) {
        this.isListing = isListing;
    }

    public String getPayOrderId() {
        return payOrderId;
    }

    public void setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
    }

    public String getPayReceipt() {
        return payReceipt;
    }

    public void setPayReceipt(String payReceipt) {
        this.payReceipt = payReceipt;
    }

}

