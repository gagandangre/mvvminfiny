package com.example.mvvm_architecture.UI.RoomDB.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.databinding.UserItemBinding;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class UserListAdapter extends ListAdapter<UserDetails, UserListAdapter.MyViewHolder> {

    Context context;
    List<UserDetails> userDetailsList;
    UpdateItem updateItem;


    private static final DiffUtil.ItemCallback<UserDetails> DIFF_CALLBACK = new DiffUtil.ItemCallback<UserDetails>() {
        @Override
        public boolean areItemsTheSame(@NonNull UserDetails oldItem, @NonNull UserDetails newItem) {
            return oldItem.getRoom_id().equals(newItem.getRoom_id()) &&
                    oldItem.getMobile().equals(newItem.getMobile()) &&
                    oldItem.getEmail().equals(newItem.getEmail());
        }

        @Override
        public boolean areContentsTheSame(@NonNull UserDetails oldItem, @NonNull UserDetails newItem) {
            return oldItem.getRoom_id().equals(newItem.getRoom_id()) &&
                     oldItem.getFirstName().equals(newItem.getFirstName()) &&
                    oldItem.getMobile().equals(newItem.getMobile()) &&
                            oldItem.getEmail().equals(newItem.getEmail());
        }
    };

    public UserListAdapter(Context context, List<UserDetails> userDetailsList, UpdateItem updateItem) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.updateItem = updateItem;
       /* this.userDetailsList = new ArrayList<>();
        this.userDetailsList = userDetailsList;
        this.context = context;
        this.updateItem = updateItem;*/
    }

    public interface UpdateItem {
        void setItemClick(UserDetails userdetails, int position);
    }

    public UserDetails getUserAt(int position) {
        return getItem(position);
    }


    @NonNull
    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new MyViewHolder(UserItemBinding.inflate(LayoutInflater.from(parent.getContext())));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MyViewHolder holder, int position) {

//        UserDetails userDetails = userDetailsList.get(position);
        UserDetails userDetails = getItem(position);
        holder.userItemBinding.txtName.setText(userDetails.getFirstName());
        holder.userItemBinding.txtEmail.setText(userDetails.getEmail());
        holder.userItemBinding.txtPhone.setText(userDetails.getMobile());

        holder.userItemBinding.txtUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                userDetails.setUser_type("update");
                updateItem.setItemClick(userDetails,position);
            }
        });

        holder.userItemBinding.txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                userDetails.setUser_type("delete");
                updateItem.setItemClick(userDetails, position);
            }
        });
    }

  /*  @Override
    public int getItemCount() {
        return userDetailsList.size();
    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        UserItemBinding userItemBinding;

        public MyViewHolder(@NonNull @NotNull UserItemBinding itemView) {
            super(itemView.getRoot());
            userItemBinding =itemView;
        }
    }
}
