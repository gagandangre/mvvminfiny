package com.example.mvvm_architecture.UI.PaginationPager3Library.Repository;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.PagingState;
import androidx.paging.rxjava2.RxPagingSource;


import com.example.mvvm_architecture.Apis.UserApi;
import com.example.mvvm_architecture.UI.Models.WalletBalanceData;
import com.example.mvvm_architecture.UI.Models.WalletTxnsModel;
import com.example.mvvm_architecture.Utils.CommonUtils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MywalletRepo extends RxPagingSource<Integer, WalletBalanceData> {


    private MutableLiveData<WalletTxnsModel> volumesResponseLiveData;
    Context context ;
    String filterType;
    int monthFilter,yearFilter,currentPage;

    public MywalletRepo() {
        volumesResponseLiveData = new MutableLiveData<>();

    }


    public void getWalletlist(Context context, String filterType, int monthFilter, int yearFilter, int perPage, int currentPage) {
        this.context = context;
        this.filterType = filterType;
        this.monthFilter = monthFilter;
        this.yearFilter = yearFilter;
        this.currentPage = currentPage;

        Log.i(getClass().getName(),"getWalletlist--->");

    }

    @NotNull
    @Override
    public Single<LoadResult<Integer, WalletBalanceData>> loadSingle(@NotNull LoadParams<Integer> loadParams) {

            Log.i(getClass().getName(),"loadSingle--->"+context);

            // If page number is already there then init page variable with it otherwise we are loading fist page
            int page = loadParams.getKey() != null ? loadParams.getKey() : 1;
            // Send request to server with page number

            UserApi userApi= CommonUtils.getApiInterface();
//            SessionManager sessionManager = new SessionManager(MvvmApplication.get().getApplicationContext());
//            SharedPreferences  sharedPreferences = MvvmApplication.get().getApplicationContext().getSharedPreferences(sessionManager.PREF_NAME,Context.MODE_PRIVATE);

            Log.i(getClass().getName(),"filterType--->"+page);
            Log.i(getClass().getName(),"filterType--->"+filterType);
            Log.i(getClass().getName(),"monthFilter--->"+monthFilter);
            Log.i(getClass().getName(),"yearFilter--->"+yearFilter);



            return  userApi.transactionsListingPager3(CommonUtils.token," ",4,2021,10,page)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(WalletTxnsModel::getData)
                    .map(walletBalanceDataArrayList -> toLoadResult(walletBalanceDataArrayList, page))
                    .onErrorReturn(LoadResult.Error::new);

    }



    private LoadResult<Integer, WalletBalanceData> toLoadResult(List<WalletBalanceData> walletBalanceDataList, int page) {

        Log.i(getClass().getName(),"toLoadResult----"+page);
        Log.i(getClass().getName(),"walletBalanceDataList----"+walletBalanceDataList.size());
        Integer prevKey,nextKey;
        if (page == 1){
            prevKey = null;
        }else {
            prevKey = page-1;
        }
        nextKey =page+1;
        Log.i(getClass().getName(),"prevKey--"+prevKey);
        Log.i(getClass().getName(),"nextKey--"+nextKey);

        return new LoadResult.Page(walletBalanceDataList, prevKey, nextKey);
    }

    @Nullable
    @Override
    public Integer getRefreshKey(@NotNull PagingState<Integer, WalletBalanceData> pagingState) {
        return null;
    }
}
