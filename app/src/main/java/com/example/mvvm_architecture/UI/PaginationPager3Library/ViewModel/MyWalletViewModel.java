package com.example.mvvm_architecture.UI.PaginationPager3Library.ViewModel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.rxjava3.PagingRx;


import com.example.mvvm_architecture.UI.Models.WalletBalanceData;
import com.example.mvvm_architecture.UI.Models.WalletTxnsModel;
import com.example.mvvm_architecture.UI.PaginationPager3Library.Repository.MywalletRepo;

import java.util.ArrayList;

import io.reactivex.rxjava3.core.Flowable;
import kotlinx.coroutines.CoroutineScope;

public class MyWalletViewModel extends ViewModel {

    WalletTxnsModel walletTxnsModel = new WalletTxnsModel();
    MywalletRepo mywalletRepo;
    LiveData<WalletTxnsModel> volumesResponseLiveData;
    public Flowable<PagingData<WalletBalanceData>> moviePagingDataFlowable;

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();


    public MutableLiveData<String> filterType = new MutableLiveData<>();
    public MutableLiveData<Integer> monthFilter = new MutableLiveData<>();
    public MutableLiveData<Integer> yearFilter = new MutableLiveData<>();


    public ArrayList<WalletBalanceData> getTransactions(){
        return  walletTxnsModel.getData();
    }

    public MyWalletViewModel() {
        isLoading.setValue(true);
        init();

    }

    public void init() {
        Log.i(getClass().getName(),"init--->");
        // Define Paging Source
        mywalletRepo = new MywalletRepo();

        // Create new Pager
        Pager<Integer, WalletBalanceData> pager = new Pager(
                // Create new paging config
                new PagingConfig(10, //  Count of items in one page
                        10, //  Number of items to prefetch
                        false, // Enable placeholders for data which is not yet loaded
                        20, // initialLoadSize - Count of items to be loaded initially
                        20 * 499),// maxSize - Count of total items to be shown in recyclerview
                () -> mywalletRepo); // set paging source




        // init Flowable
        moviePagingDataFlowable = PagingRx.getFlowable(pager);
        CoroutineScope coroutineScope = ViewModelKt.getViewModelScope(this);
        PagingRx.cachedIn(moviePagingDataFlowable, coroutineScope);

    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    public  void getWalletlist(Context context,String filterType,int monthFilter,int yearFilter,int perPage,int currentPage){
//        mywalletRepo.getWalletlist(context,filterType,monthFilter,yearFilter,perPage,currentPage);
        Log.i(getClass().getName(),"getWalletlist==="+mywalletRepo);

        MywalletRepo mywalletRepo1 = new MywalletRepo();

        Pager<Integer, WalletBalanceData> pager = new Pager(
                // Create new paging config
                new PagingConfig(10, //  Count of items in one page
                        10, //  Number of items to prefetch
                        false, // Enable placeholders for data which is not yet loaded
                        20, // initialLoadSize - Count of items to be loaded initially
                        20 * 499),// maxSize - Count of total items to be shown in recyclerview
                () -> mywalletRepo1); // set paging source




        // init Flowable
        moviePagingDataFlowable = PagingRx.getFlowable(pager);
        CoroutineScope coroutineScope = ViewModelKt.getViewModelScope(this);
        PagingRx.cachedIn(moviePagingDataFlowable, coroutineScope);

    }

    public LiveData<WalletTxnsModel> getVolumesResponseLiveData() {
        return volumesResponseLiveData;
    }

    public MutableLiveData<Integer> getMonthFilter() {
        return monthFilter;
    }

    public void setMonthFilter(MutableLiveData<Integer> monthFilter) {
        this.monthFilter = monthFilter;
    }

    public MutableLiveData<Integer> getYearFilter() {
        return yearFilter;
    }

    public void setYearFilter(MutableLiveData<Integer> yearFilter) {
        this.yearFilter = yearFilter;
    }






}
