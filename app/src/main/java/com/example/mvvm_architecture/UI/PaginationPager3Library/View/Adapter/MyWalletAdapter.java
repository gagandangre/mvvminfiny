package com.example.mvvm_architecture.UI.PaginationPager3Library.View.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mvvm_architecture.UI.Models.WalletBalanceData;
import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.Utils.CommonUtils;
import com.example.mvvm_architecture.databinding.MyWalletItemBinding;

import org.jetbrains.annotations.NotNull;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;


public class MyWalletAdapter extends PagingDataAdapter<WalletBalanceData,MyWalletAdapter.MyViewHolder> {

    Context context;
    List<WalletBalanceData> balanceDataArrayList;
    String filterType;

    public MyWalletAdapter(@NotNull DiffUtil.ItemCallback<WalletBalanceData> diffCallback, Context context) {
        super(diffCallback);
        this.context =context;
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyWalletAdapter.MyViewHolder(MyWalletItemBinding.inflate(LayoutInflater.from(parent.getContext())));
    }


    public void refreshData(String str,List<WalletBalanceData> balanceList){

        Log.i(getClass().getName(),"str----"+str);
        Log.i(getClass().getName(),"balanceList----"+balanceList.size());

        filterType =str;
        this.balanceDataArrayList.clear();
        this.balanceDataArrayList.addAll(balanceList);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        WalletBalanceData walletBalanceData = getItem(position);

        Log.i(getClass().getName(), "getTransaction_type---" + walletBalanceData.getTransaction_type());

        holder.myWalletItemBinding.txtDate.setText(CommonUtils.convertDateFormat(walletBalanceData.getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "dd/MM/yyyy"));


        if (walletBalanceData.getTransaction_type().equalsIgnoreCase("deposit")) {

            holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.red));
            holder.myWalletItemBinding.txtRupees.setText("- ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));
            holder.myWalletItemBinding.txtWalletDesc.setText("Credited in your wallet");

        } else if (walletBalanceData.getTransaction_type().equalsIgnoreCase("wallet")) {

            holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.green));
            holder.myWalletItemBinding.txtRupees.setText("+ ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));
            holder.myWalletItemBinding.txtWalletDesc.setText("Credited in your wallet");




        }else {
            holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.red));
            holder.myWalletItemBinding.txtRupees.setText("- ₹" + walletBalanceData.getClientTxn_amt());
            holder.myWalletItemBinding.txtRupees.setText("- ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));

            DecimalFormat df = new DecimalFormat("00.##");
            df.setRoundingMode(RoundingMode.DOWN);


            if(walletBalanceData.getCallId()!=null){
                double minutes =Integer.valueOf(walletBalanceData.getCallId().getAstrologerDuration()) / 60;
                double seconds =Integer.valueOf(walletBalanceData.getCallId().getAstrologerDuration()) % 60;
            }

            Log.i(getClass().getName(),"getTransaction_type-----"+walletBalanceData.getTransaction_type());

            if(walletBalanceData.getTransaction_type().equalsIgnoreCase("audio")){

                if(walletBalanceData.getCallId()!=null){
                    double minutes =Integer.valueOf(walletBalanceData.getCallId().getAstrologerDuration()) / 60;
                    double seconds =Integer.valueOf(walletBalanceData.getCallId().getAstrologerDuration()) % 60;



                    holder.myWalletItemBinding.txtWalletDesc.setText("Debited for audio call with "+walletBalanceData.getAstrologer_id().getFirstName()+", "+
                            "duration "+df.format(minutes)+":"+df.format(seconds)+" minutes");
                }


            }else if(walletBalanceData.getTransaction_type().equalsIgnoreCase("chat")){





                if(walletBalanceData.getChatId()!=null){
                    Log.i(getClass().getName(),"chat--->"+df.format(walletBalanceData.getChatId().getChatDuration()));
                    DecimalFormat df1 = new DecimalFormat("##");
                    df1.setRoundingMode(RoundingMode.DOWN);
                    int duration =Integer.parseInt(df1.format(walletBalanceData.getChatId().getChatDuration()));

                    setDuration(walletBalanceData,duration, holder.myWalletItemBinding.txtWalletDesc);

                }

            }else if(walletBalanceData.getTransaction_type().equalsIgnoreCase("video")){

                if(walletBalanceData.getCallId()!=null){
                    double minutes =Integer.valueOf(walletBalanceData.getCallId().getAstrologerDuration()) / 60;
                    double seconds =Integer.valueOf(walletBalanceData.getCallId().getAstrologerDuration()) % 60;



                    holder.myWalletItemBinding.txtWalletDesc.setText("Debited for video call with "+walletBalanceData.getAstrologer_id().getFirstName()+", "+
                            "duration "+df.format(minutes)+":"+df.format(seconds)+" minutes");
                }

            }else if(walletBalanceData.getTransaction_type().equalsIgnoreCase("report")){

                if(walletBalanceData.getPay_type()!=null && walletBalanceData.getPay_type().equalsIgnoreCase("refund")){
                    holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.green));
                    holder.myWalletItemBinding.txtWalletDesc.setText("Credited as refund for cancelling a report order");
                    holder.myWalletItemBinding.txtRupees.setText("+ ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));
                }else {
                    holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.red));
                    holder.myWalletItemBinding.txtWalletDesc.setText("Debited for placing a report order ");
                    holder.myWalletItemBinding.txtRupees.setText("- ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));
                }

            }else if (walletBalanceData.getTransaction_type().equalsIgnoreCase("product")){
                if (walletBalanceData.getProductOrderId()!=null){
                    if (walletBalanceData.getProductOrderId().getCurrentStatus()!=null&&walletBalanceData.getProductOrderId().getCurrentStatus().equals("Cancelled")){


                        String suffix=walletBalanceData.getProducts().size()>1?"products":"product";
                        if(walletBalanceData.getPay_type()!=null && walletBalanceData.getPay_type().equalsIgnoreCase("refund")){
                            holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.green));
                            holder.myWalletItemBinding.txtRupees.setText("+ ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));
                            holder.myWalletItemBinding.txtWalletDesc.setText("Credited as refund for cancelling an order for "+walletBalanceData.getProducts().size()+" "+suffix);
                        }else {
                            holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.red));
                            holder.myWalletItemBinding.txtRupees.setText("- ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));

                            if(walletBalanceData.getPaymentType().equalsIgnoreCase("wallet")){
                                holder.myWalletItemBinding.txtWalletDesc.setText("Debited for placing an order for "+walletBalanceData.getProducts().size()+" "+suffix);

                            }else {
                                holder.myWalletItemBinding.txtWalletDesc.setText("Spent for placing an order for "+walletBalanceData.getProducts().size()+" "+suffix);
                            }

                        }

                    return;
                    }
                }

//                int item=0;
//                for (ProductsItem product:walletBalanceData.getProducts()){
////                    item=item+(int)product.getQuantity();
//                    item++;
//                }
                String suffix=walletBalanceData.getProducts().size()>1?"products":"product";
                if(walletBalanceData.getPaymentType().equalsIgnoreCase("wallet")){
                    holder.myWalletItemBinding.txtWalletDesc.setText("Debited for placing an order for "+walletBalanceData.getProducts().size()+" "+suffix);

                }else {
                    holder.myWalletItemBinding.txtWalletDesc.setText("Spent for placing an order for "+walletBalanceData.getProducts().size()+" "+suffix);
                }
            }else if (walletBalanceData.getTransaction_type().equalsIgnoreCase("service")){

                Log.i(getClass().getName(),"getServiceReqId---->"+walletBalanceData.getServiceReqId());
                if(walletBalanceData.getServiceReqId()!=null){

                    if(walletBalanceData.getPay_type()!=null && walletBalanceData.getPay_type().equalsIgnoreCase("refund")){
                        holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.green));
                        holder.myWalletItemBinding.txtRupees.setText("+ ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));
                        if(walletBalanceData.getServiceReqId().getServiceId()!=null) {
                            holder.myWalletItemBinding.txtWalletDesc.setText("Credited as refund for cancelling a service request for " + walletBalanceData.getServiceReqId().getServiceId().getName() + " ");

                        }
                    }else {
                        holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.red));
                        holder.myWalletItemBinding.txtRupees.setText("- ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));

                        if(walletBalanceData.getPaymentType().equalsIgnoreCase("wallet")){
                            if(walletBalanceData.getServiceReqId().getServiceId()!=null) {
                                holder.myWalletItemBinding.txtWalletDesc.setText("Debited for placing a service request for " + walletBalanceData.getServiceReqId().getServiceId().getName());
                            }
                        }else {
                            if (walletBalanceData.getServiceReqId().getServiceId() != null) {
                                holder.myWalletItemBinding.txtWalletDesc.setText("Spent for placing a service request for " + walletBalanceData.getServiceReqId().getServiceId().getName());
                            }
                        }
                    }
                }


            }else if (walletBalanceData.getTransaction_type().equalsIgnoreCase("tip")){

                holder.myWalletItemBinding.txtRupees.setTextColor(context.getResources().getColor(R.color.red));
                holder.myWalletItemBinding.txtRupees.setText("- ₹" + CommonUtils.convertTwoDecimal(walletBalanceData.getClientTxn_amt()));
                if(walletBalanceData.getPaymentType().equalsIgnoreCase("wallet")){
                    holder.myWalletItemBinding.txtWalletDesc.setText("Debited for sending Guru Dakshina to " + walletBalanceData.getAstrologer_id().getFirstName());
                }else {
                    holder.myWalletItemBinding.txtWalletDesc.setText("Spent for sending Guru Dakshina to " + walletBalanceData.getAstrologer_id().getFirstName());
                }
            }

        }
    }

    private void setDuration(WalletBalanceData walletBalanceData,int s,TextView txtDescription) {

        String duration ="";
        int sec =  s % 60;
        int min = (s / 60)%60;
        int hours = (s/60)/60;

        Log.i(getClass().getName(),"hours--->"+hours);
        Log.i(getClass().getName(),"min--->"+min);
        Log.i(getClass().getName(),"sec--->"+sec);

        if(hours>0){
            duration =hours+" h "+min+" minutes" + sec+" sec";
        }else if(min>0){

            if(min<9){
                if(sec<9){
                    Log.i(getClass().getName(),"sec--else->"+sec);
                    duration ="0"+min+":"+"0"+sec+" minutes";
                }else {
                    Log.i(getClass().getName(),"sec--else->"+sec);
                    duration ="0"+min+":"+sec+" minutes";
                }


            }else {
                if(sec<9){
                    Log.i(getClass().getName(),"sec--else->"+sec);
                    duration =min+":"+"0"+sec+" minutes";
                }else {
                    Log.i(getClass().getName(),"sec--else->"+sec);
                    duration =min+":"+sec+" minutes";
                }
            }


        }else if(sec>0){


            if(sec<9){

                duration ="00:"+"0"+sec+" minutes";
            }else {
                duration ="00:"+sec+" minutes";
            }

        }



        txtDescription.setText("Debited for chat with "+walletBalanceData.getAstrologer_id().getFirstName()+", "+
                "duration "+duration);

    }

   /* @Override
    public int getItemCount() {
        Log.i(getClass().getName(),"getItemCount---->");
        return balanceDataArrayList.size();
    }*/

    public class MyViewHolder extends RecyclerView.ViewHolder {

        MyWalletItemBinding myWalletItemBinding;

        public MyViewHolder(@NonNull MyWalletItemBinding myWalletItemBinding) {
            super(myWalletItemBinding.getRoot());
            this.myWalletItemBinding =myWalletItemBinding;
            
        }
    }
}
