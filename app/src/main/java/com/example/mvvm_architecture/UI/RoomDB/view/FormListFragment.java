package com.example.mvvm_architecture.UI.RoomDB.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.RoomDB.viemodel.FormViewModel;
import com.example.mvvm_architecture.UI.RoomDB.view.EditUser.EditUserFragment;
import com.example.mvvm_architecture.databinding.FragmentFormListBinding;

import java.util.ArrayList;
import java.util.List;


public class FormListFragment extends Fragment implements UserListAdapter.UpdateItem {

    FragmentFormListBinding formListBinding;
    private View view;
    private Context context;
    UserListAdapter userListAdapter;
    private FormViewModel formViewModel;
    List<UserDetails> userDetailsList;
    private LinearLayoutManager linearLayoutManager;
    private Dialog dialog;
    private int position;

    public FormListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setInit();
        setAdapter();

        return view;
    }

    private void setAdapter() {
        userListAdapter = new UserListAdapter(context,userDetailsList,this);
        linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        formListBinding.recyclerForm.setLayoutManager(linearLayoutManager);
        formListBinding.recyclerForm.setAdapter(userListAdapter);
        formListBinding.recyclerForm.setHasFixedSize(true);
    }


    private void setInit() {
        formListBinding = FragmentFormListBinding.inflate(getLayoutInflater());
        view = formListBinding.getRoot();
        context = getActivity();
        userDetailsList = new ArrayList<>();
        formViewModel = new ViewModelProvider(this).get(FormViewModel.class);

        formViewModel.getAllusersList().observe(getViewLifecycleOwner(), new Observer<List<UserDetails>>() {
            @Override
            public void onChanged(List<UserDetails> userList) {
                Log.i(getClass().getName(),"userList---->"+userList.size());
                /*userDetailsList.clear();
                userDetailsList.addAll(userList);*/

                userListAdapter.submitList(userList);
                Log.i(getClass().getName(),"userDetails---->"+userDetailsList.size());
//                userListAdapter.notifyDataSetChanged();

                Log.i(getClass().getName(),"position---->"+position);
            }
        });

    }

    @Override
    public void setItemClick(UserDetails userdetails, int position) {

        Log.i(getClass().getName(),"userdetails--->"+userdetails.getFirstName());
        this.position  = position;

        if(userdetails!=null){
            if(userdetails.getUser_type().equals("delete")){

                deletePopup(userdetails);

            }else {
                Bundle args = new Bundle();
                args.putSerializable("userDetails", userdetails);


                EditUserFragment editUserFragment =
                        EditUserFragment.newInstance(getActivity());
                editUserFragment.setArguments(args);

                editUserFragment.show(getChildFragmentManager(),
                        "edit_details_bottomsheet");
            }

        }

    }

    private void deletePopup(UserDetails userdetails) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.double_btn_popup);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
        TextView txtSubTitle = (TextView) dialog.findViewById(R.id.txtSubTitle);
        TextView txtYes = (TextView) dialog.findViewById(R.id.txtYes);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);

        txtCancel.setText("No");
        txtYes.setText("YES");

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                formViewModel.delete(userdetails);
                dialog.dismiss();
            }
        });

        dialog.show();


    }
}