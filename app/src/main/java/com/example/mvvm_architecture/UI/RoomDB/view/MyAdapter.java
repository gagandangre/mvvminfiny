package com.example.mvvm_architecture.UI.RoomDB.view;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MyAdapter extends FragmentStateAdapter {

    private ArrayList<Fragment> arrayList = new ArrayList<>();

    public MyAdapter(@NonNull @NotNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }


    @NonNull
    @NotNull
    @Override
    public Fragment createFragment(int position) {
        Log.i(getClass().getName(),"position--->"+position);
        if (position == 0) {
            return new FormFragment();
        }
        return new FormListFragment();
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
