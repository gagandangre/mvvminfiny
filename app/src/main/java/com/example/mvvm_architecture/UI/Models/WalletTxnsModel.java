package com.example.mvvm_architecture.UI.Models;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class WalletTxnsModel implements Serializable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("error")
    @Expose
    private Boolean error;


    @SerializedName("total_count")
    @Expose
    private Integer total_count;

    @SerializedName("wallet_balance")
    @Expose
    private Double walletBalance;


    public Double getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(Double walletBalance) {
        this.walletBalance = walletBalance;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }


    public ArrayList<WalletBalanceData> getData() {
        Log.i(getClass().getName(),"getData----"+data);
        return data;
    }

    public void setData(ArrayList<WalletBalanceData> data) {
        this.data = data;
    }

    @SerializedName("data")
    @Expose
    private ArrayList<WalletBalanceData> data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "WalletTxnsModel{" +
                "title='" + title + '\'' +
                ", error=" + error +
                ", total_count=" + total_count +
                ", walletBalance=" + walletBalance +
                ", data=" + data +
                '}';
    }
}

