package com.example.mvvm_architecture.UI.Pagination.View;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.mvvm_architecture.UI.Pagination.Model.BlogDetailModel;
import com.example.mvvm_architecture.Utils.CommonUtils;
import com.example.mvvm_architecture.databinding.BlogItemBinding;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


public  class BlogAdapter extends RecyclerView.Adapter<BlogAdapter.MyViewHolder> {

    Context context;
    List<BlogDetailModel> blogDetailModelArrayList;


    public BlogAdapter(Context context, List<BlogDetailModel> blogDetailModelArrayList) {
        this.context = context;
        this.blogDetailModelArrayList = new ArrayList<>();
        Log.i(getClass().getName(),"BlogAdapter--->"+blogDetailModelArrayList);
        this.blogDetailModelArrayList = blogDetailModelArrayList;
        Log.i(getClass().getName(),"BlogAdapter--after->"+ this.blogDetailModelArrayList);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(BlogItemBinding.inflate(LayoutInflater.from(parent.getContext())));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        BlogDetailModel blogDetailModel = blogDetailModelArrayList.get(position);

        Log.i(getClass().getName(),"onBindViewHolder---->"+blogDetailModel.getName());

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width


        int deviceheight = (int)  (displaymetrics.heightPixels / 4.5);


        holder.blogItemBinding.imgBanner.getLayoutParams().height = deviceheight;


        blogDetailModel.setStr("blog_detail_screen");
        holder.blogItemBinding.relWhole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(blogDetailModel);
            }
        });


//        String img = TwilioChatApplication.get().getBaseURL()+blogDetailModel.getImage()+".jpg";

        String img = "https://astrowize.infiny.dev/"+blogDetailModel.getImage()+".jpg";

        Log.i(getClass().getName(),"img----------------"+img);
        Picasso.get().load(img)
                .into(holder.blogItemBinding.imgBanner);

        holder.blogItemBinding.txtDate.setText(CommonUtils.convertDateFormat(blogDetailModel.getCreatedAt()
                ,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                ,"dd MMM yyyy"));

        holder.blogItemBinding.txtBlogTitle.setText(blogDetailModel.getName());

    }


    @Override
    public int getItemCount() {

        Log.i(getClass().getName(),"getItemCount--->"+blogDetailModelArrayList);
        return blogDetailModelArrayList.size();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        BlogItemBinding blogItemBinding;

        public MyViewHolder(BlogItemBinding blogItemBinding) {
            super(blogItemBinding.getRoot());
            this.blogItemBinding = blogItemBinding;
        }
    }
}