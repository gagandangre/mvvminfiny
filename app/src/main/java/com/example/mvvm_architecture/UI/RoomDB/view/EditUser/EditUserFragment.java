package com.example.mvvm_architecture.UI.RoomDB.view.EditUser;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.RoomDB.viemodel.FormViewModel;
import com.example.mvvm_architecture.databinding.FragmentEditUserBinding;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class EditUserFragment extends BottomSheetDialogFragment {


    FragmentEditUserBinding fragmentEditUserBinding;
    Context context;
    View view;
    UserDetails userDetails;
    FormViewModel formViewModel;
    private Bundle bundle;


    public EditUserFragment(Context context) {
        this.context = context;
    }

    public static EditUserFragment newInstance(Context context) {
        return new EditUserFragment(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setInit();
        setListeners();

        return view;
    }

    private void setListeners() {

        fragmentEditUserBinding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                formViewModel.update(fragmentEditUserBinding.edtFirstName.getText().toString(),
                        fragmentEditUserBinding.edtEmail.getText().toString(),
                        fragmentEditUserBinding.edtPhoneNumber.getText().toString(),userDetails.getRoom_id());

                dismiss();
            }
        });
    }

    private void setInit() {
        context = getActivity();
        fragmentEditUserBinding = FragmentEditUserBinding.inflate(getLayoutInflater());
        view = fragmentEditUserBinding.getRoot();

        bundle = this.getArguments();
        userDetails = new UserDetails();
        userDetails = (UserDetails) bundle.getSerializable("userDetails");


        fragmentEditUserBinding.edtFirstName.setText(userDetails.getFirstName());
        fragmentEditUserBinding.edtEmail.setText(userDetails.getEmail());
        fragmentEditUserBinding.edtPhoneNumber.setText(userDetails.getMobile());

        formViewModel = new ViewModelProvider(getActivity()).get(FormViewModel.class);

        formViewModel.getRoomDbToastMessage().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String toastMessage) {
                if(toastMessage!=null){
                    Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
                }


            }
        });



    }
}