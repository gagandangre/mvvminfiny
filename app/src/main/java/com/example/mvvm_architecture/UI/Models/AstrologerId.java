package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AstrologerId implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("device_token")
    @Expose
    private List<Object> deviceToken = null;
    @SerializedName("wallet_balance")
    @Expose
    private Integer walletBalance;
    @SerializedName("languages_spoken")
    @Expose
    private List<String> languagesSpoken = null;
    @SerializedName("specialities")
    @Expose
    private List<String> specialities = null;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("isVerifiedEmail")
    @Expose
    private Boolean isVerifiedEmail;
    @SerializedName("isVerifiedPhone")
    @Expose
    private Boolean isVerifiedPhone;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("experience_years")
    @Expose
    private Double experienceYears;
    @SerializedName("unique_name")
    @Expose
    private String uniqueName;
    @SerializedName("user_location")
    @Expose
    private List<Double> userLocation = null;
    @SerializedName("chat_rate")
    @Expose
    private Integer chatRate;
    @SerializedName("video_rate")
    @Expose
    private Integer videoRate;
    @SerializedName("audio_rate")
    @Expose
    private Double audioRate;
    @SerializedName("report_rate")
    @Expose
    private Integer reportRate;
    @SerializedName("client_report_rate")
    @Expose
    private Integer clientReportRate;
    @SerializedName("client_chat_rate")
    @Expose
    private Double clientChatRate;
    @SerializedName("client_video_rate")
    @Expose
    private Double clientVideoRate;
    @SerializedName("is_chat")
    @Expose
    private Boolean isChat;
    @SerializedName("is_video")
    @Expose
    private Boolean isVideo;
    @SerializedName("is_audio")
    @Expose
    private Boolean isAudio;
    @SerializedName("is_report")
    @Expose
    private Boolean isReport;
    @SerializedName("certifications")
    @Expose
    private List<Object> certifications = null;
    @SerializedName("assigned_services")
    @Expose
    private List<Object> assignedServices = null;
    @SerializedName("groupId")
    @Expose
    private String groupId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("is_blocked")
    @Expose
    private Boolean isBlocked;
    @SerializedName("ratings")
    @Expose
    private List<String> ratings = null;
    @SerializedName("notification_count")
    @Expose
    private Integer notificationCount;
    @SerializedName("subscribed_consumers")
    @Expose
    private List<String> subscribedConsumers = null;
    @SerializedName("astrologer_status")
    @Expose
    private String astrologerStatus;
    @SerializedName("approval_status")
    @Expose
    private String approvalStatus;
    @SerializedName("refer_code_blocked")
    @Expose
    private Boolean referCodeBlocked;
    @SerializedName("profile_url")
    @Expose
    private String profileUrl;
    private final static long serialVersionUID = -5837072198902274997L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Object> getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(List<Object> deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Integer getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(Integer walletBalance) {
        this.walletBalance = walletBalance;
    }

    public List<String> getLanguagesSpoken() {
        return languagesSpoken;
    }

    public void setLanguagesSpoken(List<String> languagesSpoken) {
        this.languagesSpoken = languagesSpoken;
    }

    public List<String> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(List<String> specialities) {
        this.specialities = specialities;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsVerifiedEmail() {
        return isVerifiedEmail;
    }

    public void setIsVerifiedEmail(Boolean isVerifiedEmail) {
        this.isVerifiedEmail = isVerifiedEmail;
    }

    public Boolean getIsVerifiedPhone() {
        return isVerifiedPhone;
    }

    public void setIsVerifiedPhone(Boolean isVerifiedPhone) {
        this.isVerifiedPhone = isVerifiedPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Double getExperienceYears() {
        return experienceYears;
    }

    public void setExperienceYears(Double experienceYears) {
        this.experienceYears = experienceYears;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }



    public List<Double> getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(List<Double> userLocation) {
        this.userLocation = userLocation;
    }

    public Integer getChatRate() {
        return chatRate;
    }

    public void setChatRate(Integer chatRate) {
        this.chatRate = chatRate;
    }

    public Integer getVideoRate() {
        return videoRate;
    }

    public void setVideoRate(Integer videoRate) {
        this.videoRate = videoRate;
    }

    public Double getAudioRate() {
        return audioRate;
    }

    public void setAudioRate(Double audioRate) {
        this.audioRate = audioRate;
    }

    public Integer getReportRate() {
        return reportRate;
    }

    public void setReportRate(Integer reportRate) {
        this.reportRate = reportRate;
    }

    public Integer getClientReportRate() {
        return clientReportRate;
    }

    public void setClientReportRate(Integer clientReportRate) {
        this.clientReportRate = clientReportRate;
    }

    public Double getClientChatRate() {
        return clientChatRate;
    }

    public void setClientChatRate(Double clientChatRate) {
        this.clientChatRate = clientChatRate;
    }

    public Double getClientVideoRate() {
        return clientVideoRate;
    }

    public void setClientVideoRate(Double clientVideoRate) {
        this.clientVideoRate = clientVideoRate;
    }

    public Boolean getIsChat() {
        return isChat;
    }

    public void setIsChat(Boolean isChat) {
        this.isChat = isChat;
    }

    public Boolean getIsVideo() {
        return isVideo;
    }

    public void setIsVideo(Boolean isVideo) {
        this.isVideo = isVideo;
    }

    public Boolean getIsAudio() {
        return isAudio;
    }

    public void setIsAudio(Boolean isAudio) {
        this.isAudio = isAudio;
    }

    public Boolean getIsReport() {
        return isReport;
    }

    public void setIsReport(Boolean isReport) {
        this.isReport = isReport;
    }

    public List<Object> getCertifications() {
        return certifications;
    }

    public void setCertifications(List<Object> certifications) {
        this.certifications = certifications;
    }

    public List<Object> getAssignedServices() {
        return assignedServices;
    }

    public void setAssignedServices(List<Object> assignedServices) {
        this.assignedServices = assignedServices;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public List<String> getRatings() {
        return ratings;
    }

    public void setRatings(List<String> ratings) {
        this.ratings = ratings;
    }

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    public List<String> getSubscribedConsumers() {
        return subscribedConsumers;
    }

    public void setSubscribedConsumers(List<String> subscribedConsumers) {
        this.subscribedConsumers = subscribedConsumers;
    }

    public String getAstrologerStatus() {
        return astrologerStatus;
    }

    public void setAstrologerStatus(String astrologerStatus) {
        this.astrologerStatus = astrologerStatus;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Boolean getReferCodeBlocked() {
        return referCodeBlocked;
    }

    public void setReferCodeBlocked(Boolean referCodeBlocked) {
        this.referCodeBlocked = referCodeBlocked;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    @Override
    public String toString() {
        return "AstrologerId{" +
                "id='" + id + '\'' +
                ", deviceToken=" + deviceToken +
                ", walletBalance=" + walletBalance +
                ", languagesSpoken=" + languagesSpoken +
                ", specialities=" + specialities +
                ", isDeleted=" + isDeleted +
                ", isVerifiedEmail=" + isVerifiedEmail +
                ", isVerifiedPhone=" + isVerifiedPhone +
                ", firstName='" + firstName + '\'' +
                ", email='" + email + '\'' +
                ", userType='" + userType + '\'' +
                ", mobile='" + mobile + '\'' +
                ", info='" + info + '\'' +
                ", password='" + password + '\'' +
                ", experienceYears=" + experienceYears +
                ", uniqueName='" + uniqueName + '\'' +
                ", userLocation=" + userLocation +
                ", chatRate=" + chatRate +
                ", videoRate=" + videoRate +
                ", audioRate=" + audioRate +
                ", reportRate=" + reportRate +
                ", clientReportRate=" + clientReportRate +
                ", clientChatRate=" + clientChatRate +
                ", clientVideoRate=" + clientVideoRate +
                ", isChat=" + isChat +
                ", isVideo=" + isVideo +
                ", isAudio=" + isAudio +
                ", isReport=" + isReport +
                ", certifications=" + certifications +
                ", assignedServices=" + assignedServices +
                ", groupId='" + groupId + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", v=" + v +
                ", isBlocked=" + isBlocked +
                ", ratings=" + ratings +
                ", notificationCount=" + notificationCount +
                ", subscribedConsumers=" + subscribedConsumers +
                ", astrologerStatus='" + astrologerStatus + '\'' +
                ", approvalStatus='" + approvalStatus + '\'' +
                ", referCodeBlocked=" + referCodeBlocked +
                ", profileUrl='" + profileUrl + '\'' +
                '}';
    }
}