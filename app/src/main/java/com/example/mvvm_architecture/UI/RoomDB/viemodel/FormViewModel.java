package com.example.mvvm_architecture.UI.RoomDB.viemodel;

import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.RoomDB.Repo.UserDetailRepo;
import com.example.mvvm_architecture.Utils.CommonUtils;

import java.util.List;

public class FormViewModel extends AndroidViewModel {

    UserDetailRepo userDetailRepo;
    LiveData<List<UserDetails>> allusersList;
    private MutableLiveData<String> toastMessageObserver = new MutableLiveData();
    private MutableLiveData<String> roomDbToastMessage = new MutableLiveData();

    public FormViewModel(Application application) {
        super(application);
        userDetailRepo = new UserDetailRepo(application);
        allusersList = userDetailRepo.getAllUserDetails();
    }


    public LiveData<List<UserDetails>> getAllusersList() {
        return allusersList;
    }

    public MutableLiveData<String> getToastMessageObserver() {
        return toastMessageObserver;
    }

    public void insert(String firstName, String email, String phoneNo) {

        if(isvalid(firstName,email,phoneNo)){

            UserDetails userDetails = new UserDetails();
            userDetails.setFirstName(firstName);
            userDetails.setEmail(email);
            userDetails.setMobile(phoneNo);
            userDetailRepo.insert(userDetails);
        }


    }

    public void delete(UserDetails userDetails){
        userDetailRepo.delete(userDetails);
    }

    public MutableLiveData<String> getRoomDbToastMessage() {
        return userDetailRepo.getQueryToastMessage();
    }

    private boolean isvalid(String firstName, String email, String phoneNo) {

        boolean isValid = true;

        if(firstName.trim().isEmpty()){
            toastMessageObserver.setValue("Please enter your name");
            isValid = false;
        }else  if(email.trim().isEmpty()){
            toastMessageObserver.setValue("Please enter your email");
            isValid = false;
        }else  if(!CommonUtils.isValidEmail(email.trim())){
            toastMessageObserver.setValue("Please enter your email");
            isValid = false;
        }else  if(phoneNo.trim().isEmpty()){
            toastMessageObserver.setValue("Please enter your phoneNo");
            isValid = false;
        }else  if(phoneNo.trim().length()<10){
            toastMessageObserver.setValue("Please enter your 10 digit phoneNo");
            isValid = false;
        }


        return isValid;
    }


    public void update(String firstName, String email, String phoneNumber, Integer room_id) {

        if(isvalid(firstName,email,phoneNumber)){

            UserDetails userDetails = new UserDetails();
            userDetails.setRoom_id(room_id);
            userDetails.setFirstName(firstName);
            userDetails.setEmail(email);
            userDetails.setMobile(phoneNumber);
            userDetailRepo.update(userDetails);
        }

    }
}
