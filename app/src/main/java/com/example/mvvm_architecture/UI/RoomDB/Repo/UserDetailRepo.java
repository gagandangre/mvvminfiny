package com.example.mvvm_architecture.UI.RoomDB.Repo;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Update;

import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.RoomDB.Dao.UserDetailsDao;
import com.example.mvvm_architecture.UI.RoomDB.Database.UserDetailsDatabase;

import java.util.List;

public class UserDetailRepo {

    UserDetailsDao userDetailsDao;
    LiveData<List<UserDetails>> allUserDetails;
    MutableLiveData<String> queryToastMessage;

    public UserDetailRepo(Application application) {
        Log.i(getClass().getName(),"application===>");
        queryToastMessage = new MutableLiveData<>();
        UserDetailsDatabase database = UserDetailsDatabase.getInstance(application);
        userDetailsDao = database.userDetailsDao();
        allUserDetails = userDetailsDao.getAllUsers();
    }

    public LiveData<List<UserDetails>> getAllUserDetails() {
        return allUserDetails;
    }

    public  void insert(UserDetails userDetails){
        new InsertNoteAsyncTask(userDetailsDao).execute(userDetails);
        Log.i(getClass().getName(),"insert--->");
//        userDetailsDao.addUser(userDetails);
    }

    public void update(UserDetails userDetails) {
        Log.i(getClass().getName(),"update--->");
        new UpdateUserAsync(userDetailsDao).execute(userDetails);

    }

    public void delete(UserDetails userDetails) {

        new deleteUserAsync(userDetailsDao).execute(userDetails);

    }

    private class InsertNoteAsyncTask extends AsyncTask<UserDetails,Void,Void> {

        UserDetailsDao userDetailsDao;

        public InsertNoteAsyncTask(UserDetailsDao userDetailsDao) {
            this.userDetailsDao = userDetailsDao;


        }

        @Override
        protected Void doInBackground(UserDetails... userDetails) {
            userDetailsDao.addUser(userDetails[0]);
            return null;
        }
    }

    private class UpdateUserAsync extends AsyncTask<UserDetails,Void,UserDetails> {

        UserDetailsDao userDetailsDa;

        public UpdateUserAsync(UserDetailsDao userDetailsDao) {
            this.userDetailsDa = userDetailsDao;
        }

        @Override
        protected UserDetails doInBackground(UserDetails... userDetails) {

            userDetailsDa.updateUser(userDetails[0]);

            return userDetails[0];
        }

        @Override
        protected void onPostExecute(UserDetails userDetails) {
            super.onPostExecute(userDetails);
            Log.i(getClass().getName(),"onPostExecute-getFirstName-->"+userDetails.getRoom_id());
            Log.i(getClass().getName(),"onPostExecute-getFirstName-->"+userDetails.getFirstName());
            Log.i(getClass().getName(),"onPostExecute-getMobile-->"+userDetails.getMobile());
            Log.i(getClass().getName(),"onPostExecute--getEmail->"+userDetails.getEmail());

            queryToastMessage.setValue("User updated successfully");

            queryToastMessage.setValue(null);
        }
    }

    public MutableLiveData<String> getQueryToastMessage() {
        Log.i(getClass().getName(),"getQueryToastMessage---->"+queryToastMessage.getValue());
        return queryToastMessage;
    }


    private class deleteUserAsync extends AsyncTask<UserDetails,Void,UserDetails>{

        UserDetailsDao userDetailsDao;

        public deleteUserAsync(UserDetailsDao userDetailsDao) {
            this.userDetailsDao = userDetailsDao;
        }


        @Override
        protected UserDetails doInBackground(UserDetails... userDetails) {
            userDetailsDao.deleteUser(userDetails[0]);
            return userDetails[0];
        }

        @Override
        protected void onPostExecute(UserDetails userDetails) {
            super.onPostExecute(userDetails);

        }
    }
}
