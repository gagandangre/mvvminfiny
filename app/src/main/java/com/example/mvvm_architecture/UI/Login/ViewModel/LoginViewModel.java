package com.example.mvvm_architecture.UI.Login.ViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvm_architecture.UI.Login.Repository.LoginRepo;
import com.example.mvvm_architecture.UI.Models.UserModel;

public class LoginViewModel extends ViewModel {

    LoginRepo loginRepo;
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    LiveData<UserModel> userModelLiveData;
    private MutableLiveData<String> toastMessageObserver = new MutableLiveData();


    public LoginViewModel() {
        init();
    }

    private void init() {
        loginRepo = new LoginRepo();
        userModelLiveData = loginRepo.getUserDataModelMutableLiveData();
    }


    public void loginApi(String phoneNo, String password){
        isLoading.setValue(true);
        if(isValid(phoneNo,password)){
            loginRepo.loginApi(phoneNo,password);
        }

    }

    private boolean isValid(String phoneNo, String password) {

        boolean isValid = true;

        if(phoneNo.trim().isEmpty()){
            isValid = false;
            toastMessageObserver.setValue("Please enter valid  Phone no.");
        }else if(phoneNo.length()<10){
            isValid = false;
            toastMessageObserver.setValue("Please enter 10 digit  Phone no.");

        }else if(password.trim().isEmpty()){
            isValid = false;
            toastMessageObserver.setValue("Please enter valid  password");
        }


        return isValid;
    }


    public MutableLiveData<Boolean> getIsLoading() {
        return loginRepo.getBooleanMutableLiveData();
    }

    public LiveData<UserModel> getUserModelLiveData() {
        return userModelLiveData;
    }

    public MutableLiveData<String> getToastMessageObserver() {
        return toastMessageObserver;
    }
}
