package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.SerializedName;

public class ProductsItem {

	@SerializedName("quantity")
	private double quantity;

	@SerializedName("rate")
	private double rate;

	@SerializedName("product_id")
	private ProductId productId;

	@SerializedName("_id")
	private String id;

	public double getQuantity(){
		return quantity;
	}

	public double getRate(){
		return rate;
	}

	public ProductId getProductId(){
		return productId;
	}

	public String getId(){
		return id;
	}
}