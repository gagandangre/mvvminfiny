package com.example.mvvm_architecture.UI.RoomDB.Dao;


import androidx.lifecycle.LiveData;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.mvvm_architecture.UI.Models.UserDetails;

import java.util.List;

@Dao
public interface UserDetailsDao {

    @Insert
    void addUser(UserDetails userDetails);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateUser(UserDetails userDetails);


    @Delete
    void deleteUser(UserDetails userDetails);

    @Query("select * from forms")
    LiveData<List<UserDetails>> getAllUsers();



}
