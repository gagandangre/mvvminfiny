package com.example.mvvm_architecture.UI.BottomNavJetPack.View.Help.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvm_architecture.UI.BottomNavJetPack.View.Help.Repository.HelpRepo;
import com.example.mvvm_architecture.UI.Models.RatingModel;

public  class HelpViewModel extends ViewModel {

    HelpRepo helpRepo;
    LiveData<RatingModel> ratingModelMutableLiveData;
    LiveData<Boolean> isFetching;


    public HelpViewModel() {
        helpRepo = new HelpRepo();
        ratingModelMutableLiveData = new MutableLiveData<>();
    }

    public void getReview(String userId){
        helpRepo.fetchReviews(userId);

    }

    public LiveData<RatingModel> getRatingModelMutableLiveData() {
        Log.i(getClass().getName(),"getRatingModelMutableLiveData--->"+helpRepo.getRatingModelMutableLiveData());
        return helpRepo.getRatingModelMutableLiveData();
    }

    public LiveData<Boolean> getIsFetching() {
        return helpRepo.getIsFetching();
    }
}
