package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.SerializedName;

public class OrderStatusItem {

	@SerializedName("date")
	private String date;

	@SerializedName("description")
	private String description;

	@SerializedName("_id")
	private String id;

	@SerializedName("status")
	private String status;

	public String getDate(){
		return date;
	}

	public String getDescription(){
		return description;
	}

	public String getId(){
		return id;
	}

	public String getStatus(){
		return status;
	}
}