package com.example.mvvm_architecture.UI.BottomNavJetPack.View.Home.View;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.lifecycle.ViewModelProvider;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.BottomNavJetPack.View.BottomNavigationActivity;
import com.example.mvvm_architecture.UI.BottomNavJetPack.View.Home.viewmodel.HomeViewModel;
import com.example.mvvm_architecture.UI.MainActivity;
import com.example.mvvm_architecture.UI.MvvmApplication;
import com.example.mvvm_architecture.UI.Pagination.ViewModel.BlogViewModel;
import com.example.mvvm_architecture.databinding.FragmentHomeBinding;




public class HomeFragment extends Fragment {


    FragmentHomeBinding fragmentHomeBinding;
    View view;
    Context context;
    HomeViewModel homeViewModel;
    SavedStateViewModelFactory savedStateViewModelFactory;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        setInit();
        setListeners();


        return view;
    }

    private void setListeners() {
        fragmentHomeBinding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(getClass().getName(),"edtName----+"+fragmentHomeBinding.edtName.getText().toString());
                homeViewModel.saveNewName(fragmentHomeBinding.edtName.getText().toString());
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        homeViewModel = ((BottomNavigationActivity)getActivity()).homeViewModel;
        Log.i(getClass().getName(),"homeViewModel-onResume--->"+homeViewModel);
        if(homeViewModel!=null){
            homeViewModel.getName().observe(getViewLifecycleOwner(), new Observer<String>() {
                @Override
                public void onChanged(String data) {
                    Log.i(getClass().getName(),"data----"+data);

                    fragmentHomeBinding.edtName.setText(data);
                }
            });
        }
    }

    private void setInit() {

        fragmentHomeBinding = FragmentHomeBinding.inflate(getLayoutInflater());
        view = fragmentHomeBinding.getRoot();
        context = getActivity();


        Log.i(getClass().getName(),"homeViewModel--->"+homeViewModel);



    }
}