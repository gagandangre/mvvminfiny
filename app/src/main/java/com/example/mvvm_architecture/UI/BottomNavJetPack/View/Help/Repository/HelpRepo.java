package com.example.mvvm_architecture.UI.BottomNavJetPack.View.Help.Repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.mvvm_architecture.Apis.UserApi;
import com.example.mvvm_architecture.UI.Models.RatingModel;
import com.example.mvvm_architecture.Utils.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpRepo {

    MutableLiveData<RatingModel> ratingModelMutableLiveData;
    MutableLiveData<Boolean> isFetching;
    UserApi userApi;

    public HelpRepo() {

        ratingModelMutableLiveData = new MutableLiveData<>();
        isFetching = new MutableLiveData<>();
        userApi = CommonUtils.getApiInterface();


    }

    public  void fetchReviews(String userId){
        isFetching.setValue(true);
        Call<RatingModel> call = userApi.getReviewsList(CommonUtils.token,userId);

        call.enqueue(new Callback<RatingModel>() {
            @Override
            public void onResponse(Call<RatingModel> call, Response<RatingModel> response) {
                Log.i(getClass().getName(),"call---->"+call);
                if (!response.isSuccessful()) {
                    Log.i("Err", "Err: " + response.code());
                } else {
                    Log.i(getClass().getName(), "call---->" + response.isSuccessful());
                    Log.i(getClass().getName(), "call---->" + response.body());
                    isFetching.setValue(false);
                    RatingModel ratingModel = response.body();

                    ratingModelMutableLiveData.setValue(ratingModel);
                    Log.i(getClass().getName(), "ratingModel---->" + ratingModel.getTitle());
                }

            }

            @Override
            public void onFailure(Call<RatingModel> call, Throwable t) {
                Log.i(getClass().getName(),"onFailure---->");
                isFetching.setValue(false);
            }
        });
    }

    public MutableLiveData<RatingModel> getRatingModelMutableLiveData() {
        return ratingModelMutableLiveData;
    }

    public MutableLiveData<Boolean> getIsFetching() {
        return isFetching;
    }
}
