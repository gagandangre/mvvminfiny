package com.example.mvvm_architecture.UI.EditProfile.Repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.mvvm_architecture.Apis.UserApi;
import com.example.mvvm_architecture.UI.Models.UserModel;
import com.example.mvvm_architecture.Utils.CommonUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class EditProfileRepo {

    UserApi userApi;
    private MutableLiveData<UserModel> userModelMutableLiveData;
    private MutableLiveData<Boolean> progressBarState;


    public EditProfileRepo() {
        userModelMutableLiveData = new MutableLiveData<>();
        progressBarState =new MutableLiveData<>();

        userApi = CommonUtils.getApiInterface();
    }


    public void editProfileApi(String firstName, String email, String phoneNumber, String imageString){
        progressBarState.postValue(true);

        userApi.editProfile(CommonUtils.token,
                firstName,
                email,
                "Consumer",
                phoneNumber,
                "4/12/1998",
                "9:20 AM",
                imageString,"male",
                "Nerul Mumbai",
                23.00,43.00)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::editProfileSuccess, this::editProfileFailure);

    }

    private void editProfileSuccess(UserModel userModel) {
        userModelMutableLiveData.postValue(userModel);
        progressBarState.postValue(false);
    }

    private void editProfileFailure(Throwable throwable) {
        progressBarState.postValue(false);
    }

    public MutableLiveData<Boolean> getProgressBarLoadingState() {
        Log.i(getClass().getName(),"getProgressBarLoadingState--->");
        return progressBarState;
    }

    public MutableLiveData<UserModel> getUserModelMutableLiveData() {
        return userModelMutableLiveData;
    }
}
