package com.example.mvvm_architecture.UI.BottomNavJetPack.View.Help.view;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.UI.BottomNavJetPack.View.Help.viewmodel.HelpViewModel;
import com.example.mvvm_architecture.UI.Models.RatingModel;
import com.example.mvvm_architecture.UI.Models.UserRating;
import com.example.mvvm_architecture.databinding.FragmentHelpBinding;
import com.example.mvvm_architecture.databinding.FragmentHomeBinding;
import com.example.mvvm_architecture.databinding.HelpItemBinding;

import java.util.ArrayList;
import java.util.List;


public class HelpFragment extends Fragment {



    public HelpFragment() {
        // Required empty public constructor
    }

    FragmentHelpBinding fragmentHelpBinding;
    View view;
    Context context;
    HelpAdapter helpAdapter;
    HelpViewModel helpViewModel;
    private LinearLayoutManager linearLayoutManager;
    List<UserRating> userRatingList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setInit();

        return view;
    }


    private void setInit() {

        fragmentHelpBinding = FragmentHelpBinding.inflate(getLayoutInflater());
        view = fragmentHelpBinding.getRoot();
        context = getActivity();
        userRatingList  = new ArrayList<>();





        helpViewModel = new ViewModelProvider(this).get(HelpViewModel.class);
        helpViewModel.getReview("605b185caf8a141d4b897acd");
        helpViewModel.getRatingModelMutableLiveData().observe(getViewLifecycleOwner(), new Observer<RatingModel>() {
            @Override
            public void onChanged(RatingModel ratingModel) {
                Log.i(getClass().getName(),"ratingModel----onChanged-----"+ratingModel.getData().getUserrating());

               /* if(ratingModel.getData()!=null && ratingModel.getData().getUserrating()!=null){
                    userRatingList = ratingModel.getData().getUserrating();
                    helpAdapter.notifyDataSetChanged();
                }*/
                userRatingList.addAll(ratingModel.getData().getUserrating());
                helpAdapter = new HelpAdapter(context,userRatingList);
                linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                fragmentHelpBinding.recyclerHelp.setLayoutManager(linearLayoutManager);
                fragmentHelpBinding.recyclerHelp.setAdapter(helpAdapter);

            }
        });



    }
}