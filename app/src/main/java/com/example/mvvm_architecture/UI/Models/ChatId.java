package com.example.mvvm_architecture.UI.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("is_deleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("chat_rate")
    @Expose
    private Double chatRate;
    @SerializedName("client_chat_rate")
    @Expose
    private Double clientChatRate;
    @SerializedName("consumer_id")
    @Expose
    private String consumerId;
    @SerializedName("astrologer_id")
    @Expose
    private String astrologerId;
    @SerializedName("request_status")
    @Expose
    private String requestStatus;
    @SerializedName("channel_name")
    @Expose
    private String channelName;
    @SerializedName("channel_id")
    @Expose
    private String channelId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("consumer_member_id")
    @Expose
    private String consumerMemberId;
    @SerializedName("astrologer_member_id")
    @Expose
    private String astrologerMemberId;
    @SerializedName("accepted_time")
    @Expose
    private String acceptedTime;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("chat_duration")
    @Expose
    private Double chatDuration;
    @SerializedName("end_reason")
    @Expose
    private String endReason;
    @SerializedName("end_time")
    @Expose
    private String endTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Double getChatRate() {
        return chatRate;
    }

    public void setChatRate(Double chatRate) {
        this.chatRate = chatRate;
    }

    public Double getClientChatRate() {
        return clientChatRate;
    }

    public void setClientChatRate(Double clientChatRate) {
        this.clientChatRate = clientChatRate;
    }

    public String getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(String consumerId) {
        this.consumerId = consumerId;
    }

    public String getAstrologerId() {
        return astrologerId;
    }

    public void setAstrologerId(String astrologerId) {
        this.astrologerId = astrologerId;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getConsumerMemberId() {
        return consumerMemberId;
    }

    public void setConsumerMemberId(String consumerMemberId) {
        this.consumerMemberId = consumerMemberId;
    }

    public String getAstrologerMemberId() {
        return astrologerMemberId;
    }

    public void setAstrologerMemberId(String astrologerMemberId) {
        this.astrologerMemberId = astrologerMemberId;
    }

    public String getAcceptedTime() {
        return acceptedTime;
    }

    public void setAcceptedTime(String acceptedTime) {
        this.acceptedTime = acceptedTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Double getChatDuration() {
        return chatDuration;
    }

    public void setChatDuration(Double chatDuration) {
        this.chatDuration = chatDuration;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
