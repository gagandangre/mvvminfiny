package com.example.mvvm_architecture.Utils;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.mvvm_architecture.R;
import com.example.mvvm_architecture.databinding.ActivityCropImageBinding;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.RejectedExecutionException;

public class CropImageActivity extends AppCompatActivity {

    private static final int CROP_ACTIVITY = 15;
    private static String path;


    
    ActivityCropImageBinding activityCropImageBinding;

    File file;
    private RectF mFrameRect = null;
    private static final String KEY_FRAME_RECT = "FrameRect";
    Context context;
    Uri mSourceUri;
    String TAG="cropImageActivityLog";
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.JPEG;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
      /*  Intent intent=new Intent(this,PaperWorkActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in,R.anim.left_out);
        finish();*/

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityCropImageBinding = ActivityCropImageBinding.inflate(getLayoutInflater());
        
        View view = activityCropImageBinding.getRoot();
        
        setContentView(view);
        
        context=this;
        if (savedInstanceState != null) {
            // restore data
            mFrameRect = savedInstanceState.getParcelable(KEY_FRAME_RECT);
        }

        Intent intent = getIntent();
        mSourceUri = Uri.parse(intent.getStringExtra("sourceUri"));
        activityCropImageBinding.cropImageView.setCropMode(CropImageView.CropMode.CIRCLE);
        Boolean fromDoc = intent.getBooleanExtra("from_documents", false);
        if(fromDoc){
            Log.i(TAG, "======CUSTOM==>" );
            activityCropImageBinding.cropImageView.setCropMode(CropImageView.CropMode.FREE);
        }/*else{
            Log.i(TAG, "=====SQUARE=====>" );
            activityCropImageBinding.cropImageView.setCropMode(CropImageView.CropMode.SQUARE);
        }*/

        Log.i(TAG, "==mSourceUri==>"+ mSourceUri);
        //activityCropImageBinding.cropImageView.setCropMode();



        activityCropImageBinding.cropImageView.load(mSourceUri)
                .initialFrameRect(mFrameRect)
                .useThumbnail(true)
                .execute(mLoadCallback);

        activityCropImageBinding.rotateRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCropImageBinding.cropImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D); // rotate clockwise by 90 degrees
            }
        });

        activityCropImageBinding.rotateLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityCropImageBinding.cropImageView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D); // rotate counter-clockwise by 90 degrees
            }
        });

        activityCropImageBinding.buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressBar.setVisibility(View.VISIBLE);
                activityCropImageBinding.cropImageView.crop(mSourceUri).execute(mCropCallback);
            }
        });

        activityCropImageBinding.buttonCencel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                setResult(2,intent);
                finish();

            }
        });

    }
    private final LoadCallback mLoadCallback = new LoadCallback() {
        @Override
        public void onSuccess() {
            activityCropImageBinding.imgProg.setVisibility(View.GONE);

        }

        @Override
        public void onError(Throwable e) {
            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();

        }
    };
    private final CropCallback mCropCallback = new CropCallback() {
        @Override
        public void onSuccess(Bitmap cropped) {
            try {
                activityCropImageBinding.cropImageView.save(cropped)
                        .compressFormat(mCompressFormat)
                        .execute(createSaveUri(), mSaveCallback);
            }catch (RejectedExecutionException e){

            }
        }

        @Override
        public void onError(Throwable e) {
        }
    };
    public Uri createSaveUri() {
        return createNewUri(context, mCompressFormat);
    }
    public static Uri createNewUri(Context context, Bitmap.CompressFormat format) {
        long currentTimeMillis = System.currentTimeMillis();
        Date today = new Date(currentTimeMillis);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String title = dateFormat.format(today);
        String dirPath = getDirPath();
        String fileName = "scv" + title + "." + getMimeType(format);
        path = dirPath + "/" + fileName;
        File file = new File(path);
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, title);
        values.put(MediaStore.Images.Media.DISPLAY_NAME, fileName);
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/" + getMimeType(format));
        values.put(MediaStore.Images.Media.DATA, path);
        long time = currentTimeMillis / 1000;
        values.put(MediaStore.MediaColumns.DATE_ADDED, time);
        values.put(MediaStore.MediaColumns.DATE_MODIFIED, time);
        if (file.exists()) {
            values.put(MediaStore.Images.Media.SIZE, file.length());
        }
        ContentResolver resolver = context.getContentResolver();
        Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//        Logger.i("SaveUri = " + uri);
        return uri;
    }

    public static String getDirPath() {
        String dirPath = "";
        File imageDir = null;
        File extStorageDir = Environment.getExternalStorageDirectory();
        if (extStorageDir.canWrite()) {
            imageDir = new File(extStorageDir.getPath() + "/simplecropview");
        }
        if (imageDir != null) {
            if (!imageDir.exists()) {
                imageDir.mkdirs();
            }
            if (imageDir.canWrite()) {
                dirPath = imageDir.getPath();
            }
        }
        return dirPath;
    }

    public static String getMimeType(Bitmap.CompressFormat format) {
//        Logger.i("getMimeType CompressFormat = " + format);
        switch (format) {
            case JPEG:
                return "jpeg";
            case PNG:
                return "png";
        }
        return "png";
    }

    private final SaveCallback mSaveCallback = new SaveCallback() {
        @Override
        public void onSuccess(Uri outputUri) {
            Intent intent= new Intent();
            Log.i(TAG, "sourceuri"+ String.valueOf(outputUri)
                    +"path_---------"+path );
            intent.putExtra("sourceuri", outputUri);
            intent.putExtra("path",path);
            setResult(RESULT_OK,intent);
            finish();



        }

        @Override
        public void onError(Throwable e) {
            Toast.makeText(CropImageActivity.this,"errror", Toast.LENGTH_SHORT).show();

        }
    };

}

