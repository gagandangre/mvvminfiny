package com.example.mvvm_architecture.UI.PaginationPager3Library.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;

import com.example.mvvm_architecture.UI.PaginationPager3Library.View.Adapter.MyWalletAdapter;
import com.example.mvvm_architecture.UI.PaginationPager3Library.View.Adapter.WalletBalanceDataCompare;
import com.example.mvvm_architecture.UI.PaginationPager3Library.View.Adapter.WalletLoadStateAdapter;
import com.example.mvvm_architecture.UI.PaginationPager3Library.ViewModel.MyWalletViewModel;
import com.example.mvvm_architecture.Utils.CommonUtils;
import com.example.mvvm_architecture.Utils.MonthYearPickerDialog;
import com.example.mvvm_architecture.databinding.ActivityWalletBinding;

import java.util.Calendar;

public class MyWalletActivity extends AppCompatActivity {

    ActivityWalletBinding  activityWalletBinding;
    Context context;
    MyWalletAdapter myWalletAdapter;
    MyWalletViewModel myWalletViewModel;
    private ProgressDialog progressBar;
    private int yearFilter;
    private int monthFilter;
    String[] mntData = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private int filterApplied;
    String filterType="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setInit();

        setListeners();

    }

    private void setListeners() {

        activityWalletBinding.txtDateSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });

        activityWalletBinding.rbAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterType = "";
            }
        });

        activityWalletBinding.rbWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterType = "wallet";

            }
        });


    }

    private void selectDate() {

        MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
        pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month2, int i2) {


                Log.i("createDialo", " createDialogWithoutDateField" + i2 +" fitlt "+ filterApplied);
                filterApplied =i2;
                if( year > 0){
                    Log.i(getClass().getName(),"month year" +year );
                    Log.i(getClass().getName(),"month month" +monthFilter );

                    monthFilter = month2;
                    yearFilter = year;
                    Log.i(getClass().getName(),"month yearC " +yearFilter );
                    Log.i(getClass().getName(),"month monthC " +monthFilter );

                    activityWalletBinding.txtDateSort.setText(mntData[monthFilter-1] + " "+ yearFilter);

                    myWalletViewModel.getWalletlist(context,filterType,monthFilter,yearFilter,10,1);
                }

            }
        },"Filter",monthFilter-1,yearFilter,filterApplied);
        pickerDialog.show(getSupportFragmentManager(), "MonthYearPickerDialog");


    }

    private void setInit() {

        activityWalletBinding = ActivityWalletBinding.inflate(getLayoutInflater());
        View view =  activityWalletBinding.getRoot();
        setContentView(view);
        context =this;

        myWalletViewModel = new ViewModelProvider(this).get(MyWalletViewModel.class);
        myWalletAdapter = new MyWalletAdapter(new WalletBalanceDataCompare(),context);

        LinearLayoutManager linearLayoutManager; linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        activityWalletBinding.recyclerMywallet.setLayoutManager(linearLayoutManager);
        activityWalletBinding.recyclerMywallet.setAdapter(
                // This will show end user a progress bar while pages are being requested from server
                myWalletAdapter.withLoadStateFooter(
                        // When we will scroll down and next page request will be sent
                        // while we get response form server Progress bar will show to end user
                        new WalletLoadStateAdapter(v -> {
                            myWalletAdapter.retry();
                        }))
        );

        // Subscribe to to paging data
        myWalletViewModel.moviePagingDataFlowable.subscribe(walletBalanceDataPagingData -> {
            // submit new data to recyclerview adapter
            Log.i(getClass().getName(),"moviePagingData---"+walletBalanceDataPagingData);
//            myWalletViewModel.isLoading.setValue(false);
            myWalletAdapter.submitData(getLifecycle(), walletBalanceDataPagingData);

            Log.i(getClass().getName(),"getItems--->"+myWalletAdapter.snapshot().getItems());


        });


        myWalletViewModel.getIsLoading().observe(this, (Boolean isLoading) -> {

            Log.i(getClass().getName(),"getIsLoading--->"+isLoading);
            if (isLoading){
//                progressBar = CommonUtils.showProgressdialog(context);
            } else {
                CommonUtils.hideLoading(progressBar);
            }

        });

        Calendar calendar = Calendar.getInstance();
        yearFilter = calendar.get(Calendar.YEAR);
        monthFilter = calendar.get(Calendar.MONTH)+1;
        Log.i("oncreate ", "== > "+monthFilter
                + " year "+ yearFilter);
        activityWalletBinding.txtDateSort.setText( mntData[monthFilter-1]+" "+ yearFilter);


    }
}