package com.example.mvvm_architecture.Apis;

import com.example.mvvm_architecture.UI.Models.RatingModel;
import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.Models.UserModel;
import com.example.mvvm_architecture.UI.Models.WalletTxnsModel;
import com.example.mvvm_architecture.UI.Pagination.Model.BlogModel;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserApi {

    @FormUrlEncoded
    @POST("blog/list")
    Observable<BlogModel> getBlogList(@Header("token") String token,
                                      @Field("perPage") Integer perPage,
                                      @Field("page") Integer page);


    @FormUrlEncoded
    @POST("transaction/transactionsListing")
    Single<WalletTxnsModel> transactionsListingPager3(@Header("token") String token,
                                                      @Field("filter_type") String filter_type,
                                                      @Field("month") Integer month,
                                                      @Field("year") Integer year,
                                                      @Field("perPage") Integer perPage,
                                                      @Field("page") Integer page);


    @FormUrlEncoded
    @POST("user/editProfile")
    Observable<UserModel> editProfile(@Header("token") String token,
                                      @Field("first_name") String firstName,
                                      @Field("email") String email,
                                      @Field("user_type") String user_type,
                                      @Field("mobile") String mobile,
                                      @Field("date_of_birth") String date_of_birth,
                                      @Field("birth_time") String birth_time,
                                      @Field("profile_url") String profile_url,
                                      @Field("gender") String gender,
                                      @Field("birth_place") String birth_place,
                                      @Field("latitude") Double latitude,
                                      @Field("longitude") Double longitude);


    @POST("user/signin")
    Observable<UserModel> userLogin(@Body UserDetails userDetails);

    @FormUrlEncoded
    @POST("user/getAstrologerRatings")
    Call<RatingModel> getReviewsList(@Header("token") String token, @Field("user_id") String name);

}
