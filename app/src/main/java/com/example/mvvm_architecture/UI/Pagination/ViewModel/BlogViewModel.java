package com.example.mvvm_architecture.UI.Pagination.ViewModel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvm_architecture.UI.Pagination.Model.BlogDetailModel;
import com.example.mvvm_architecture.UI.Pagination.Model.BlogModel;
import com.example.mvvm_architecture.UI.Pagination.Repositories.BlogRepository;

import java.util.ArrayList;
import java.util.List;

public class BlogViewModel extends ViewModel {

    public boolean isScrolling;
    public int currentItems, totalItems, scrollOutItems , currentPage=1;
    BlogRepository blogRepository;
    LiveData<BlogModel> blogModelLiveData;
    private int perPage=4;
    String token;

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private List<BlogDetailModel> blogDetailModelArrayList;
    private int totalCount;

    public BlogViewModel(){
        init();
    }

    private void init() {

        blogDetailModelArrayList = new ArrayList<>();
        blogRepository = new BlogRepository();
        blogModelLiveData = blogRepository.getBlogModelMutableLiveData();


    }


    public void fetchBlogsApi(String token) {
        isLoading.setValue(true);
        blogRepository.fetchBlogsApi(token, perPage,currentPage);
    }


    public LiveData<BlogModel> getBlogModelLiveData() {

        Log.i(getClass().getName(),"currentPage--->"+currentPage);
        if(currentPage ==1){
            blogDetailModelArrayList.clear();
        }

        if(blogModelLiveData.getValue()!=null && blogModelLiveData.getValue().getError()!=null){
            Log.i(getClass().getName(),"blogModelLiveData--->"+blogModelLiveData.getValue().getError());
            if(!blogModelLiveData.getValue().getError()){
                totalCount = blogModelLiveData.getValue().getTotalCount();

                if(blogModelLiveData.getValue().getData().size()>0){
                    return blogModelLiveData;
                }else {

                }

            }
        }


        return blogModelLiveData;
    }


    public MutableLiveData<Boolean> getIsLoading() {
        return blogRepository.getProgressBarLoadingState();
    }

    public void recyclerScrollingEvent(List<BlogDetailModel> blogDetailModelArrayList, int dx, int dy, int totalCount) {

        this.blogDetailModelArrayList = blogDetailModelArrayList;
        this.totalCount = totalCount;
        Log.i(getClass().getName(),"blogDetailModelArrayList--->"+blogDetailModelArrayList.size());
        Log.i(getClass().getName(),"dy--->"+dy);

        Boolean isScrolled = false;
        if(blogDetailModelArrayList.size()< totalCount){
            isScrolled = true;
        }
        Log.i(getClass().getName(),"isScrolled--->"+isScrolled);

        Log.i(getClass().getName(),"isScrolling--->"+isScrolling);
        Log.i(getClass().getName(),"currentItems--->"+currentItems);
        Log.i(getClass().getName(),"scrollOutItems--->"+scrollOutItems);
        Log.i(getClass().getName(),"totalItems--->"+totalItems);

        if(isScrolled && dy>0 && isScrolling && (currentItems + scrollOutItems == totalItems )) {
            isScrolling = false;
            currentPage++;
            Log.i(getClass().getName(),"isScrolling--->");
            isLoading.setValue(true);
            blogRepository.fetchBlogsApi(token, perPage,currentPage);        
        }
    }
}
