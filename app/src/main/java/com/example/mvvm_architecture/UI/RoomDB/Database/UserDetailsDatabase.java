package com.example.mvvm_architecture.UI.RoomDB.Database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.mvvm_architecture.UI.Models.UserDetails;
import com.example.mvvm_architecture.UI.MvvmApplication;
import com.example.mvvm_architecture.UI.RoomDB.Dao.UserDetailsDao;

@Database(entities = UserDetails.class,version = 2, exportSchema = false)
public abstract class UserDetailsDatabase extends RoomDatabase {

    private static UserDetailsDatabase instance;

    public abstract UserDetailsDao userDetailsDao();

    public static synchronized UserDetailsDatabase getInstance(Context context){

        if(instance==null){

            instance = Room.databaseBuilder(context,UserDetailsDatabase.class,"userdetails_data")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallBack)
                    .build();

        }

        return instance;
    }

    private static RoomDatabase.Callback roomCallBack = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            Log.i(getClass().getName(),"roomCallBack--->");
            // new PopulateDb(instance).execute();
        }
    };

}
